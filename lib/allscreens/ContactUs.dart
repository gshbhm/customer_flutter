import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/AppMessages.dart';
import 'package:shopkeeperapp/constants/CustomLoader.dart';
import 'package:shopkeeperapp/constants/ReusableWidgets.dart';
import 'package:shopkeeperapp/constants/UniversalFunctions.dart';
import 'package:shopkeeperapp/constants/ValidatorFunctions.dart';
import 'package:shopkeeperapp/constants/memory_management.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _phoneController = new TextEditingController();
  final TextEditingController _subjectController = new TextEditingController();
  final TextEditingController _messageController = new TextEditingController();

  final FocusNode _nameFocusNode = new FocusNode();
  final FocusNode _emailFocusNode = new FocusNode();
  final FocusNode _phoneFocusNode = new FocusNode();
  final FocusNode _subjectFocusNode = new FocusNode();
  final FocusNode _messageFocusNode = new FocusNode();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool _validate = false;

  CustomLoader _customLoader = CustomLoader();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: GradientAppBar(
          title: Text(
            "Shopkeeper App",
            style: TextStyle(color: Colors.white),
            textAlign: TextAlign.center,
          ),
          backgroundColorStart: ColorConstants.kGradientTop,
          backgroundColorEnd: ColorConstants.kGradientBottom,
        ),
        body: Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    getSpacer(height: 40),
                    _getNameField,
                    getSpacer(height: 20),
                    _getEmailField,
                    getSpacer(height: 20),
                    _getPhoneField,
                    getSpacer(height: 20),
                    _getSubjectField,
                    getSpacer(height: 20),
                    _getMessageField,
                    getSpacer(height: 20),
                    _getSubmitButton,
                    getSpacer(
                      height: getScreenSize(context: context).height * 0.02,
                    )
                  ],
                ),
              ),
            )),
      ),
    );
  }

  // Returns name field
  get _getNameField {
    return textFieldWidget(
        context: context,
        label: "Name",
        focusNode: _nameFocusNode,
        controller: _nameController,
        validator: (value) {
          return usernameValidator(name: value, context: context);
        },
        keyboardType: TextInputType.text,
        onFieldSubmitted: (val) {
          setFocusNode(context: context, focusNode: _emailFocusNode);
        },
        inputAction: TextInputAction.next,
        icon: Icon(
          FontAwesomeIcons.userFriends,
          size: 20,
        ));
  }

  get _getEmailField {
    return textFieldWidget(
        context: context,
        label: "Email",
        focusNode: _emailFocusNode,
        controller: _emailController,
        validator: (value) {
          return emailValidator(email: value, context: context);
        },
        keyboardType: TextInputType.emailAddress,
        onFieldSubmitted: (val) {
          setFocusNode(context: context, focusNode: _phoneFocusNode);
        },
        inputAction: TextInputAction.next,
        icon: Icon(
          FontAwesomeIcons.envelopeOpen,
          size: 20,
        ));
  }

  get _getPhoneField {
    return textFieldWidget(
        context: context,
        label: "Phone Number",
        focusNode: _phoneFocusNode,
        controller: _phoneController,
        validator: (value) {
          return phoneNumberValidator(phoneNumber: value, context: context);
        },
        keyboardType: TextInputType.phone,
        onFieldSubmitted: (val) {
          setFocusNode(context: context, focusNode: _subjectFocusNode);
        },
        inputAction: TextInputAction.next,
        icon: Icon(
          FontAwesomeIcons.phoneAlt,
          size: 20,
        ));
  }

  get _getSubjectField {
    return textFieldWidget(
        context: context,
        label: "Subject",
        focusNode: _subjectFocusNode,
        controller: _subjectController,
        validator: (value) {
          return subjectValidator(val: value, context: context);
        },
        keyboardType: TextInputType.text,
        onFieldSubmitted: (val) {
          setFocusNode(context: context, focusNode: _messageFocusNode);
        },
        inputAction: TextInputAction.next,
        icon: Icon(
          FontAwesomeIcons.info,
          size: 20,
        ));
  }

  get _getMessageField {
    return textFieldWidget(
      context: context,
      label: "Message",
      focusNode: _messageFocusNode,
      controller: _messageController,
      validator: (value) {
        return messageValidator(val: value, context: context);
      },
      keyboardType: TextInputType.multiline,
      maxLines: 3,
      inputAction: TextInputAction.done,
      icon: Icon(
        FontAwesomeIcons.stickyNote,
        size: 20,
      ),
    );
  }

  Widget textFieldWidget({
    String label,
    Icon icon,
    @required TextEditingController controller,
    @required BuildContext context,
    @required FocusNode focusNode,
    TextInputAction inputAction,
    Function(String) onFieldSubmitted,
    TextInputType keyboardType,
    Function(String) validator, int maxLines,
  }) {
    return Theme(
      data: ThemeData(
        primaryColor: AppColors.kGreen,
      ),
      child: TextFormField(
        controller: controller,
        style: TextStyle(color: Colors.black),
        cursorColor: AppColors.kGreen,
        onFieldSubmitted: onFieldSubmitted,
        keyboardType: keyboardType,
        validator: validator,
        maxLines:maxLines ,
        textInputAction: inputAction ?? TextInputAction.next,
        decoration: InputDecoration(
            prefixIcon: icon,
            hintText: label,
            // errorText: _validate ? '$validator' : null,
            border: OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.kGreen),
                borderRadius: BorderRadius.all(Radius.circular(16.0)))),
      ),
    );
  }

  // Returns login button

  get _getSubmitButton {
    return getAppFlatButton(
      context: context,
      backGroundColor: AppColors.kGreen,
      titleText: "Submit",
      onPressed: () {
        setState(() {
          closeKeyboard(context: context, onClose: () {});
          if (_formKey.currentState.validate()) {
            _contactUsApi();
          }
        });
      },
    );
  }

  _contactUsApi() async {
    Map<String, String> body = {
      "name": _nameController.text.trim(),
      "email": _emailController.text.trim(),
      "phone": _phoneController.text.trim(),
      "subject": _subjectController.text.trim(),
      "message": _messageController.text.trim()
    };
    _customLoader.showLoader(context);

    String url = kContactUs;
    print(url);
    var response = await APIServices.postMethod(context, kContactUs, body);
    print(response);
    if (response["status"] == 200) {
      setState(() {
        getToast(msg: response["message"]);
        _customLoader.hideLoader();
        _nameController.clear();
        _emailController.clear();
        _phoneController.clear();
        _subjectController.clear();
        _messageController.clear();
      });
    } else if (response["status"] == 422) {
      _customLoader.hideLoader();
      getToast(msg: response["message"]);
    }
  }
}
