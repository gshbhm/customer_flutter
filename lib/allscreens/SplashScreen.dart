import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shopkeeperapp/allscreens/Dashboard.dart';
import 'package:shopkeeperapp/allscreens/HomeScreen.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/constants/memory_management.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

bool _appInitiated = false;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

double lat, lng;

class _SplashScreenState extends State<SplashScreen> {
  Position _currentPosition;

  @override
  void initState() {
    super.initState();
    _setDefaults();
    _getCurrentLocation();
    print("***********************************");
    /* print(MemoryManagement.getAccessToken());
    print(MemoryManagement.getLogin());*/
    // accessToken = MemoryManagement.getAccessToken();
    // isLogin = MemoryManagement.getLogin();
  }

  @override
  Widget build(BuildContext context) {
    return /*WillPopScope(
      child:*/
        new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.1,
                0.4,
                0.9
              ],
              colors: [
                ColorConstants.kGradientTop,
                ColorConstants.kGradientCenter,
                ColorConstants.kGradientBottom,
              ]),
          shape: BoxShape.rectangle,
        ),
        child: Center(
          child: Container(
            alignment: Alignment.center,
            width: 100.0,
            height: 100.0,
            child: Icon(
              FontAwesomeIcons.appStore,
              size: 140.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        lat = _currentPosition.latitude;
        lng = _currentPosition.longitude;
        print("$_currentPosition.latitude,$_currentPosition.longitude");
      });
    }).catchError((e) {
      print(e);
    });
  }

  void _setDefaults() async {
    bool sharedPrefsInitialized = await MemoryManagement.init();
    startTime();
  }

  startTime() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, () async {
      _appInitiated = true;
      setState(() {});
      _goToLoginScreen();
    });
  }

  _goToLoginScreen() async {
    String token = MemoryManagement.getAccessToken();
    print("token : $token");
    if ((token?.trim() ?? "") == "") {
      Navigator.pushAndRemoveUntil(
          context,
          CupertinoPageRoute(
              builder: (context) => new HomeScreen(true, lat, lng)),
          (Route<dynamic> route) => false);
      //login
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          CupertinoPageRoute(
              builder: (context) => new HomeScreen(false, lat, lng)),
          (Route<dynamic> route) => false);
    }
  }
}
