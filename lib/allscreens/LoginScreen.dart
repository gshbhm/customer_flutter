import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:shopkeeperapp/allscreens/AccountScreen.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/AppMessages.dart';
import 'package:shopkeeperapp/constants/CustomLoader.dart';
import 'package:shopkeeperapp/constants/ReusableWidgets.dart';
import 'package:shopkeeperapp/constants/UniversalFunctions.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/constants/ValidatorFunctions.dart';
import 'package:shopkeeperapp/constants/memory_management.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;

import 'HomeScreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // Focus Nodes
  final FocusNode _emailFocusNode = new FocusNode();
  final FocusNode _passwordFocusNode = new FocusNode();

  final TextEditingController passwordController = new TextEditingController();
  final TextEditingController emailController = new TextEditingController();

  // Global keys
  final GlobalKey<FormState> _loginFormKey = new GlobalKey<FormState>();
  bool _validate = false;

  CustomLoader _customLoader = CustomLoader();
  Duration animationDuration = Duration(milliseconds: 270);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        /*appBar: GradientAppBar(
          title: Text(
            "Shopkeeper App",
            style: TextStyle(color: Colors.white),
            textAlign: TextAlign.center,
          ),
          backgroundColorStart: ColorConstants.kGradientTop,
          backgroundColorEnd: ColorConstants.kGradientBottom,
        ),*/
        body: Form(
            key: _loginFormKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(bottom: 62, top: 16),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 3.8,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(190),
                              bottomRight: Radius.circular(190)),
                          gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              stops: [
                                0.1,
                                0.4,
                                0.9
                              ],
                              colors: [
                                ColorConstants.kGradientTop,
                                ColorConstants.kGradientCenter,
                                ColorConstants.kGradientBottom,
                              ])),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: AnimatedOpacity(
                          opacity: 1.0,
                          duration: animationDuration,
                          child: GestureDetector(
                            child: Container(
                              child: Text(
                                'LOGIN'.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 32,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontFamily: 'Raleway'),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    getSpacer(height: 40),
                    _getEmailField,
                    getSpacer(height: 20),
                    _getPasswordField,
                    getSpacer(height: 20),
                    _getSubmitButton,
                    getSpacer(
                      height: getScreenSize(context: context).height * 0.02,
                    )
                  ],
                ),
              ),
            )),
      ),
    );
  }

  get _getEmailField {
    return textFieldWidget(
        context: context,
        label: "Email",
        focusNode: _emailFocusNode,
        controller: emailController,
        validator: (value) {
          return emailValidator(email: value, context: context);
        },
        keyboardType: TextInputType.emailAddress,
        onFieldSubmitted: (val) {
          setFocusNode(context: context, focusNode: _passwordFocusNode);
        },
        inputAction: TextInputAction.next,
        obscureText: false,
        icon: Icon(
          FontAwesomeIcons.envelopeOpen,
          size: 20,
        ));
  }

  get _getPasswordField {
    return textFieldWidget(
        context: context,
        label: "Password",
        focusNode: _passwordFocusNode,
        controller: passwordController,
        validator: (value) {
          return passwordValidator(newPassword: value, context: context);
        },
        keyboardType: TextInputType.emailAddress,
        inputAction: TextInputAction.done,
        obscureText: true,
        icon: Icon(
          Icons.vpn_key,
          size: 20,
        ));
  }

  Widget textFieldWidget({
    String label,
    Icon icon,
    @required TextEditingController controller,
    @required BuildContext context,
    @required FocusNode focusNode,
    TextInputAction inputAction,
    Function(String) onFieldSubmitted,
    TextInputType keyboardType,
    Function(String) validator,
    bool obscureText,
  }) {
    return Theme(
      data: ThemeData(
        primaryColor: AppColors.kGreen,
      ),
      child: TextFormField(
        controller: controller,
        style: TextStyle(color: Colors.black),
        cursorColor: AppColors.kGreen,
        onFieldSubmitted: onFieldSubmitted,
        obscureText: obscureText,
        keyboardType: keyboardType,
        validator: validator,
        textInputAction: inputAction ?? TextInputAction.next,
        decoration: InputDecoration(
            prefixIcon: icon,
            hintText: label,
            // errorText: _validate ? '$validator' : null,
            border: OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.kGreen),
                borderRadius: BorderRadius.all(Radius.circular(16.0)))),
      ),
    );
  }

  // Returns login button

  get _getSubmitButton {
    return getAppFlatButton(
      context: context,
      backGroundColor: AppColors.kGreen,
      titleText: "Login",
      onPressed: () {
        setState(() {
          closeKeyboard(context: context, onClose: () {});
          if (_loginFormKey.currentState.validate()) {
            _loginFormKey.currentState.save();
            _loginApi();
          }
        });
      },
    );
  }

  _loginApi() async {
    Map<String, String> body = {
      "email": "${emailController.text}",
      "password": "${passwordController.text}"
    };
    _customLoader.showLoader(context);
    String url = kLogin;
    print(url);
    var response = await APIServices.postMethod(context, kLogin, body);
    print(response);
    if (response["status"] == 200) {
      setState(() {
        getToast(msg: response["message"]);
        _customLoader.hideLoader();
        emailController.clear();
        passwordController.clear();
        String token = "${response['token']}";
        print("bloc Staus:$token");
        String msg = response['message'] != null ? response['message'] : "";
        String name = response["name"];
        //   String name = response['name'] != null ? response['name'] : "";
        String email = response["email"];
        //  String email = response['name'] != null ? response['email'] : "";
        print(msg);
        if (token.isNotEmpty) {
          /*await*/ MemoryManagement.init();
          MemoryManagement.setAccessToken(accessToken: token);
          MemoryManagement.setName(name: name);
          MemoryManagement.setEmail(email: email);
          print("$name$email");
          MemoryManagement.setLogin(isLogin: true);
          print("***************Login*****************");
          print(MemoryManagement.getLogin());
          _customLoader.hideLoader();

          Navigator.pushAndRemoveUntil(
              context,
              CupertinoPageRoute(builder: (context) => new HomeScreen(false, 0,0)),
              (Route<dynamic> route) => false);
        }
      });
    } else if (response["status"] == 422) {
      _customLoader.hideLoader();
      getToast(msg: response["message"]);
    }
  }
}
