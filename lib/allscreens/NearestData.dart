import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopkeeperapp/Bloc/models/MostSaleResponse.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailView.dart';
import 'package:shopkeeperapp/constants/AppMessages.dart';
import 'package:shopkeeperapp/constants/CustomLoader.dart';
import 'package:shopkeeperapp/constants/ReusableWidgets.dart';
import 'package:shopkeeperapp/constants/UniversalFunctions.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/webservices/constants.dart';
import 'package:http/http.dart' as http;

class RecentData extends StatefulWidget {
  String title;

  RecentData(this.title);

  @override
  _RecentDataState createState() => _RecentDataState();
}

CustomLoader _customLoader = CustomLoader();
MostSaleResponse mostSaleResponse = new MostSaleResponse();
List<String> _imageslist = [
  "assets/coffeeshop.png",
  "assets/Beer.jpg",
  "assets/burgerking.jpg",
  "assets/clothes.jpg",
  "assets/fastfood.jpg",
  "assets/flowershop.jpg",
  "assets/kfc.jpg",
  "assets/lacoste.jpg",
  "assets/nike.jpg",
  "assets/subway.jpg",
];
List<String> _shopNameList = [
  "Coffee Shop",
  "Beer Shop",
  "Burger King",
  "Garments",
  "FastFood",
  "Flower Shop",
  "KFC",
  "Lacoste",
  "Nike",
  "Subway"
];

List<MostSaleResponse> mostSaleList = [];

class _RecentDataState extends State<RecentData> {
  @override
  void initState() {
    super.initState();
    //if (widget.title == "Most Sale Shops") {
    _getMostSale();
    // }
  }

  Future<void> _getMostSale() async {
    bool isConnected = await isConnectedToInternet();
    if (!isConnected ?? true) {
      _customLoader.hideLoader();
      getToast(msg: AppMessages.noInternetError);
      return;
    }
    String url = kNearest + "?skip=0&take=2&lat=1.55555&long=75.22222";
    print(url);
    // _customLoader.showLoader(context);

    final response =
        await http.get(url, headers: {"Accept": "application/json"});
    print(response.body);

    if (response.statusCode == 200) {
      //  _customLoader.hideLoader();

      print(response.statusCode);
      var result = json.decode(response.body);
      mostSaleResponse = MostSaleResponse.fromJson(result);
      mostSaleList.add(MostSaleResponse.fromJson(result));
      print("mostsale+${mostSaleResponse.data.length}");
      setState(() {
        /* var result = json.decode(response.body);
        mostSaleResponse = MostSaleResponse.fromJson(result);
        mostSaleList.add(MostSaleResponse.fromJson(result));
        print("mostsale+${mostSaleResponse.data.length}");*/
      });
    } else {
      // Utils.showErrorAlert('Error', response['message'], context);
/*
      getToast(msg: "Something went wrong");
      throw Exception('Failed to load transactions');*/
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: new MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              color: Colors.white,
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
            backgroundColor: Colors.purple,
            centerTitle: true,
            title: Text(
              widget.title,
              style: TextStyle(color: Colors.white),
            ),
          ),
          body: ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              Column(
                children: <Widget>[_getList3(context)],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _getList3(BuildContext context) {
    return FutureBuilder(
      future: _getMostSale(),
      builder: (ctx, datasnapshot) => datasnapshot.connectionState ==
              ConnectionState.waiting
          ? Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              child: Center(
                child: Text('No data available'),
              ),
            )
          : Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: mostSaleResponse.data.length,
                itemBuilder: (context, index) {
                  var mostSaleList = mostSaleResponse.data[index];
                  int ratio = mostSaleList.ratio;
                  print(mostSaleResponse.data.length);
                  return GestureDetector(
                      onTap: () {
/*
                        Utils.push(context, ShopDetailView(index));
*/
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 100.0,
                        padding: EdgeInsets.only(left: 15.0),
                        color: Colors.white,
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 15.0, left: 15.0),
                              width: 100.0,
                              height: 80.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: mostSaleList.image.isNotEmpty
                                      ? NetworkImage(kBaseUserProfile +
                                          mostSaleList.image[1].image)
                                      : AssetImage("assets/dummy_bg.jpg"),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 115,
                              child: ListTile(
                                title: Row(
                                  //mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      child: RichText(
                                        textAlign: TextAlign.start,
                                        text: new TextSpan(
                                          style: DefaultTextStyle.of(context)
                                              .style,
                                          children: <TextSpan>[
                                            TextSpan(
                                                text: mostSaleList.name != null
                                                    ? mostSaleList.name
                                                    : '',
                                                style: new TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.black)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                                subtitle: Row(
                                  //crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      child: RichText(
                                        text: new TextSpan(
                                          style: DefaultTextStyle.of(context)
                                              .style,
                                          children: <TextSpan>[
                                            TextSpan(
                                                text:
                                                    mostSaleList.address != null
                                                        ? mostSaleList.address
                                                        : '',
                                                style: new TextStyle(
                                                    fontSize: 13.0,
                                                    color: Colors.grey)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                trailing: Container(
                                  width: 20.0,
                                  height: 15.0,
                                  margin: EdgeInsets.only(top: 6.0),
                                  padding: EdgeInsets.all(1.0),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: Colors.green,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(3.0)),
                                  ),
                                  alignment: Alignment.bottomRight,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        ratio != null ? '$ratio' : '',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12.0),
                                      ),
                                      Icon(
                                        Icons.star,
                                        color: Colors.white,
                                        size: 10,
                                      ),
                                    ],
                                  ),
                                ),
                                // isThreeLine: true,
                              ),
                            ),
                          ],
                        ),
                      ));
                },
              ),
            ),
    );
  }
}
