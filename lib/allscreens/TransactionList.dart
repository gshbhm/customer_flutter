import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopkeeperapp/Bloc/models/TransactionResponse.dart';
import 'package:http/http.dart' as http;
import 'package:shopkeeperapp/constants/CustomLoader.dart';

class TransactionList extends StatefulWidget {
  String name, description, istDate;
  int amount, finalAmount, discount;

  TransactionList(
    this.name,
    this.description,
    this.istDate,
    this.amount,
    this.discount,
    this.finalAmount,
  );

  @override
  _TransactionListState createState() => _TransactionListState();
}

CustomLoader _customLoader = CustomLoader();
List<TransactionResponse> transactionList = [];

class _TransactionListState extends State<TransactionList> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: new MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              color: Colors.white,
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
            backgroundColor: Colors.purple,
            centerTitle: true,
            title: Text(
              'Transaction Detail',
              style: TextStyle(color: Colors.white),
            ),
          ),
          body: ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              Column(
                children: <Widget>[
                  _getList(
                      context,
                      widget.name,
                      widget.description,
                      widget.istDate,
                      widget.amount,
                      widget.finalAmount,
                      widget.discount)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }
}

Widget _getList(BuildContext context, String name, String description,
    String isDate, int amount, int finalAmount, int discount) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: MediaQuery.of(context).size.height,
    child: ListView.builder(
      //controller: ,
      //padding: EdgeInsets.all(10.0),
      scrollDirection: Axis.vertical,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 1,
      itemBuilder: (context, index) {
        return Wrap(
          /* width: MediaQuery.of(context).size.width,
          height: 180.0,
          alignment: Alignment.center,
          color: Colors.white,*/
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    child: Container(
                      padding: EdgeInsets.all(12.0),
                      child: Column(
                        children: <Widget>[
                          /*Text(_shopNameList[index],
                                  style: TextStyle(
                                      color: Colors.purple,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold)),*/
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 18.0),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Shop : ",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 18.0),
                                child: Text(
                                  name,
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 18.0,
                          ),
                          SizedBox(
                            height: 1.0,
                            child: Container(
                              color: Colors.grey.shade300,
                            ),
                          ),
                          SizedBox(
                            height: 18.0,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Description : ",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Flexible(
                                child: Text(
                                  description,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 16.0),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 18.0,
                          ),
                          SizedBox(
                            height: 1.0,
                            child: Container(
                              color: Colors.grey.shade300,
                            ),
                          ),
                          SizedBox(
                            height: 18.0,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Date : ",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Flexible(
                                child: Text(
                                  isDate,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 16.0),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 18.0,
                          ),
                          SizedBox(
                            height: 1.0,
                            child: Container(
                              color: Colors.grey.shade300,
                            ),
                          ),
                          /*SizedBox(
                            height: 18.0,
                          ),*/
                          Container(
                            color: Colors.grey.shade200,
                            padding: EdgeInsets.only(
                                left: 6.0, top: 12.0, bottom: 12.0),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Bill Details : ",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            height: 3.0,
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: 6.0, top: 8.0, bottom: 8.0, right: 6.0),
                            color: Colors.grey.shade400,
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Total Amount",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                      /* fontWeight: FontWeight.bold*/
                                    ),
                                  ),
                                ),
                                Flexible(
                                  child: Text(
                                    '₹ $amount',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16.0),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: 6.0, top: 8.0, bottom: 8.0, right: 6.0),
                            color: Colors.grey.shade400,
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Discount",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                      /* fontWeight: FontWeight.bold*/
                                    ),
                                  ),
                                ),
                                Flexible(
                                  child: Text(
                                    '- ₹ $discount',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16.0),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: 6.0, top: 8.0, bottom: 8.0, right: 6.0),
                            color: Colors.lightGreen,
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Total Paid Amount",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                      /* fontWeight: FontWeight.bold*/
                                    ),
                                  ),
                                ),
                                Flexible(
                                  child: Text(
                                    '₹ $finalAmount',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16.0),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                        ],
                      ),
                    )) /* Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: SizedBox(
                        width: 350.0,
                        child:
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5,
                    margin: EdgeInsets.all(10),
                  ),
                ),*/
              ],
            ),
          ],
        );
      },
    ),
  );
}
