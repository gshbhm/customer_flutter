import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shopkeeperapp/Bloc/models/TransactionResponse.dart';
import 'package:shopkeeperapp/allscreens/TransactionList.dart';
import 'package:shopkeeperapp/constants/AppMessages.dart';
import 'package:shopkeeperapp/constants/CustomLoader.dart';
import 'package:shopkeeperapp/constants/ReusableWidgets.dart';
import 'package:shopkeeperapp/constants/UniversalFunctions.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/constants/app_manager.dart';
import 'package:shopkeeperapp/constants/memory_management.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/authStoreUtils.dart';
import 'package:shopkeeperapp/webservices/constants.dart';
import 'package:http/http.dart' as http;

class PaymentList extends StatefulWidget {
  @override
  _PaymentListState createState() => _PaymentListState();
}

CustomLoader _customLoader = CustomLoader();
List<TransactionResponse> transactionList = [];

Future<String> _calculation = Future<String>.delayed(
  Duration(seconds: 2),
  () => 'Data Loaded',
);

class _PaymentListState extends State<PaymentList> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            color: Colors.white,
            onPressed: () {
              setState(() {
                Navigator.pop(context);
              });
            },
            icon: Icon(Icons.arrow_back_ios),
          ),
          backgroundColor: Colors.purple,
          centerTitle: true,
          title: Text(
            'Transaction List',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: FutureBuilder<String>(
            future: _calculation,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView(
                  scrollDirection: Axis.vertical,
                  children: <Widget>[
                    Column(
                      children: <Widget>[_getList(context)],
                    )
                  ],
                );
              } else if (snapshot.hasError) {
                return Center(
                    child: Text("${snapshot.error}",
                        style: Theme.of(context).textTheme.headline));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    getTransactionServer();
  }

  @override
  void dispose() {
    super.dispose();
    transactionList.clear();
  }

  getTransactionServer() async {
    String userToken = MemoryManagement.getAccessToken();
    print('asdasdasd   $userToken');

    String body = '?' +
        kUserToken +
        '=' +
        userToken +
        '=' +
        '&' +
        kSkip +
        '=' +
        '0' +
        '&' +
        kTake +
        '=' +
        '6';
    print(body);
    var response = await APIServices.getMethod(context, kTransaction, body);
    print(response);
    if (response != null) {
      setState(() {
        List<dynamic> listData = response;
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = TransactionResponse.fromJson(dict);
          transactionList.add(model);
          var logger = Logger();
          logger.d("Length ${transactionList.length}");
        }
      });
    } else {
      Utils.showErrorAlert('Error', response['message'], context);
    }
  }
}

Widget _getList(BuildContext context) {
  return transactionList != null
      ? Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: ListView.builder(
            //controller: ,
            //padding: EdgeInsets.all(10.0),
            scrollDirection: Axis.vertical,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: transactionList.length,
            itemBuilder: (context, index) {
              var transactionDetail = transactionList[index];
              String storename = transactionDetail.store.name != null
                  ? transactionDetail.store.name
                  : '';
              String description = transactionDetail.store.description != null
                  ? transactionDetail.store.name
                  : '';
              String date = transactionDetail.istDate != null
                  ? transactionDetail.istDate
                  : '';
              int finalamount = transactionDetail.finalAmount != null
                  ? transactionDetail.finalAmount
                  : '';

              int discount = transactionDetail.discount != null
                  ? transactionDetail.discount
                  : '';
              int amount = transactionDetail.amount != null
                  ? transactionDetail.amount
                  : '';
              return GestureDetector(
                onTap: () {
                  Utils.pushReplacement(
                      context,
                      TransactionList(storename, description, date, amount,
                          discount, finalamount));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 80.0,
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: 15.0),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: ListTile(
                          title: Row(
                            //mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: RichText(
                                  textAlign: TextAlign.start,
                                  text: new TextSpan(
                                    style: DefaultTextStyle.of(context).style,
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: storename,
                                          style: new TextStyle(
                                              fontSize: 18.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          subtitle: Row(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: RichText(
                                  text: new TextSpan(
                                    style: DefaultTextStyle.of(context).style,
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: 'Amount Paid: $finalamount',
                                        style: new TextStyle(
                                            fontSize: 13.0, color: Colors.grey),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          trailing: Image.asset(
                            'assets/paid.jpg',
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(right: 10.0),
                        height: 1.0,
                        color: Colors.grey.shade300,
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        )
      : Container(
          child: Text("No Data Available"),
        );
}
