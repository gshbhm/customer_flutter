import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shopkeeperapp/Bloc/models/MostSaleResponse.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailView.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailViewNew.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';

String name = '', address = '', image = '';
int rating;

class MostSaleData extends StatefulWidget {
  final String title;

  MostSaleData(this.title);

  @override
  _MostSaleDataState createState() => _MostSaleDataState();
}

Data data;
List<Data> mostSaleList = [];
int pageNo = 0;

class _MostSaleDataState extends State<MostSaleData> {
  @override
  void initState() {
    super.initState();
    _mostSaleShopsApi();
  }

  @override
  void dispose() {
    super.dispose();
    mostSaleList.clear();
  }

  _mostSaleShopsApi() async {
    String body = '?' +
        kSkip +
        '=' +
        '$pageNo' +
        '&' +
        kTake +
        '=' +
        '6' +
        '&' +
        kLat +
        '=' +
        '29.685629' +
        '&' +
        kLong +
        '=' +
        '76.990547';
    print(body);
    // onLoading(context);
    var response = await APIServices.getMethod(context, kMostSale, body);
    print(response);
    if (response['data'] != null) {
      setState(() {
        List<dynamic> listData = response['data'];
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = Data.fromJson(dict);
          mostSaleList.add(model);
        }
        //  Navigator.pop(context);
      });
    }
  }

  Future<bool> _onWillPop() async {
    Navigator.pop(context);
  }

  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
    () => 'Data Loaded',
  );

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: new MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              color: Colors.white,
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
            backgroundColor: Colors.purple,
            centerTitle: true,
            title: Text(
              widget.title,
              style: TextStyle(color: Colors.white),
            ),
          ),
          body: FutureBuilder<String>(
              future: _calculation,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView(
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      Column(
                        children: <Widget>[_getList3(context)],
                      )
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Center(
                      child: Text("${snapshot.error}",
                          style: Theme.of(context).textTheme.headline));
                } else {
                  return Center(child:CircularProgressIndicator());
                }
              }),
        ),
      ),
    );
  }

  Widget _getList3(BuildContext context) {
    return mostSaleList != null
        ? Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: mostSaleList.length,
              itemBuilder: (context, index) {
                var mostSaleListData = mostSaleList[index];
                int ratio = mostSaleListData.ratio;
                print("length$mostSaleList.length");
                return GestureDetector(
                    onTap: () {
                      Utils.push(
                        context,
                        ShopDetailViewNew(mostSaleListData.id)
                        /*ShopDetailView(
                            mostSaleListData.image != null
                                ? mostSaleListData.image[0].image
                                : AssetImage("assets/dummy_bg.jpg"),
                             mostSaleListData.image != null
                                  ? mostSaleListData.image
                                  : '',
                            mostSaleListData.name,
                            mostSaleListData.address,
                            mostSaleListData.phone,
                            ratio,
                            mostSaleListData.email != null
                                ? mostSaleListData.email
                                : "",
                            mostSaleListData.cityObj != null
                                ? mostSaleListData.cityObj.title
                                : "",
                            mostSaleListData.stateObj != null
                                ? mostSaleListData.stateObj.title
                                : "")*/
                        ,
                      );
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 100.0,
                      padding: EdgeInsets.only(left: 15.0),
                      color: Colors.white,
                      child: Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 15.0, left: 15.0),
                            width: 100.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              image: new DecorationImage(
                                fit: BoxFit.fill,
                                image: mostSaleListData.image.isNotEmpty
                                    ? NetworkImage(kBaseUserProfile +
                                        mostSaleListData.image[0].image)
                                    : AssetImage("assets/dummy_bg.jpg"),
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 115,
                            child: ListTile(
                              title: Row(
                                //mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: RichText(
                                      textAlign: TextAlign.start,
                                      text: new TextSpan(
                                        style:
                                            DefaultTextStyle.of(context).style,
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  mostSaleListData.name != null
                                                      ? mostSaleListData.name
                                                      : '',
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colors.black)),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              subtitle: Row(
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: RichText(
                                      text: new TextSpan(
                                        style:
                                            DefaultTextStyle.of(context).style,
                                        children: <TextSpan>[
                                          TextSpan(
                                              text: mostSaleListData.address !=
                                                      null
                                                  ? mostSaleListData.address
                                                  : '',
                                              style: new TextStyle(
                                                  fontSize: 13.0,
                                                  color: Colors.grey)),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              // Todo jatin here
                              trailing: Container(
                                width: 20.0,
                                height: 15.0,
                                margin: EdgeInsets.only(top: 6.0),
                                padding: EdgeInsets.all(1.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.green,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3.0)),
                                ),
                                alignment: Alignment.bottomRight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      ratio != null ? '$ratio' : '',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12.0),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                  ],
                                ),
                              ),
                              // isThreeLine: true,
                            ),
                          ),
                        ],
                      ),
                    ));
              },
            ),
          )
        : Container(
            alignment: Alignment.center,
            child: Text('No Data Available'),
          );
  }

  onLoading(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          child: SpinKitCubeGrid(
            color: ColorConstants.mainColor1,
            size: 50.0,
          ),
        );
      },
    );
  }
}
