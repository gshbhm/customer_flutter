/*
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:shopkeeperapp/Bloc/models/NearestResponseModel.dart';
import 'package:shopkeeperapp/allscreens/HighRateData.dart';
import 'package:shopkeeperapp/allscreens/MostSaleData.dart';
import 'package:shopkeeperapp/allscreens/RecentData.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailViewNew.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';

class Dashboard extends StatefulWidget {
  double lat, lng;
  Dashboard(this.lat, this.lng);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  List<Data> nearestList = [];
  List<Data> mostSaleList = [];
  List<Data> recentList = [];
  List<Data> highRateList = [];

  @override
  void initState() {
    super.initState();
    _recentShopsApi();
    _mostSaleShopsApi();
    _nearestShopsApi();
    _highRateShopsApi();
  }

  @override
  void dispose() {
    super.dispose();
    return null;
  }

  _nearestShopsApi() async {
    String body = '?' +
        kSkip +
        '=' +
        '0' +
        '&' +
        kTake +
        '=' +
        '4' +
        '&' +
        kLat +
        '=' +
        '${widget.lat}' +
        '&' +
        kLong +
        '=' +
        '${widget.lng}';
    print(body);
    var response = await APIServices.getMethod(context, kNearest, body);
    print(response);
    if (response['data'] != null) {
      setState(() {
        List<dynamic> listData = response['data'];
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = Data.fromJson(dict);
          nearestList.add(model);
        }
      });
    } else {
      Utils.showErrorAlert('Error', response['message'], context);
    }
  }

  _mostSaleShopsApi() async {
    String body = '?' +
        kSkip +
        '=' +
        '0' +
        '&' +
        kTake +
        '=' +
        '4' +
        '&' +
        kLat +
        '=' +
        '${widget.lat}' +
        '&' +
        kLong +
        '=' +
        '${widget.lng}';
    print(body);

    var response = await APIServices.getMethod(context, kMostSale, body);
    print(response);
    if (response['data'] != null) {
      setState(() {
        List<dynamic> listData = response['data'];
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = Data.fromJson(dict);
          mostSaleList.add(model);
        }
      });
    }
  }

  _recentShopsApi() async {
    String body = '?' +
        kSkip +
        '=' +
        '0' +
        '&' +
        kTake +
        '=' +
        '2' +
        '&' +
        kLat +
        '=' +
        '${widget.lat}' +
        '&' +
        kLong +
        '=' +
        '${widget.lng}';
    print(body);
    var response = await APIServices.getMethod(context, kRecent, body);
    print(response);
    if (response['data'] != null) {
      setState(() {
        List<dynamic> listData = response['data'];
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = Data.fromJson(dict);
          recentList.add(model);
        }
      });
    } else {
      Utils.showErrorAlert('Error', response['message'], context);
    }
  }

  _highRateShopsApi() async {
    String body = '?' +
        kSkip +
        '=' +
        '0' +
        '&' +
        kTake +
        '=' +
        '3' +
        '&' +
        kLat +
        '=' +
        '${widget.lat}' +
        '&' +
        kLong +
        '=' +
        '${widget.lng}';
    print(body);
    var response = await APIServices.getMethod(context, kHighRated, body);
    print(response);
    if (response['data'] != null) {
      setState(() {
        List<dynamic> listData = response['data'];
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = Data.fromJson(dict);
          highRateList.add(model);
        }
      });
    } else {
      Utils.showErrorAlert('Error', response['message'], context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return */
/*WillPopScope(
      onWillPop: () async => true,
      child: new*//*

      MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: GradientAppBar(
            title: Text(
              "Shopkeeper App",
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.center,
            ),
            backgroundColorStart: ColorConstants.kGradientTop,
            backgroundColorEnd: ColorConstants.kGradientBottom,
          ),
          body: ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(12.0, 13.0, 12.0, 13.0),
                    width: MediaQuery.of(context).size.width,
                    height: 160.0,
                    color: Colors.white,
                    child: Center(
                      child: Stack(children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 0.0),
                          child: Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: SizedBox(
                                height: 200.0,
                                width: 350.0,
                                child: Carousel(
                                  images: [
                                    AssetImage("assets/Beer.jpg"),
                                    AssetImage("assets/burgerking.jpg"),
                                    AssetImage("assets/clothes.jpg"),
                                  ],
                                  dotSize: 4.0,
                                  dotSpacing: 15.0,
                                  dotColor: Colors.white,
                                  indicatorBgPadding: 5.0,
                                  dotBgColor: Colors.purple.withOpacity(0.5),
                                  borderRadius: true,
                                  moveIndicatorFromBottom: 180.0,
                                  noRadiusForIndicator: true,
                                )),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            elevation: 5,
                            //  margin: EdgeInsets.all(10),
                          ),
                        ),
                      ]),
                    ),
                  ),
                  SafeArea(
                    child: (Container(
                      height: 8.0,
                    )),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Padding(
                          padding:
                          EdgeInsets.only(left: 15.0, top: 15.0, right: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Nearest Shops',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  _getImages(context),
                  SafeArea(
                    child: (Container(
                      height: 8.0,
                    )),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Padding(
                            padding: EdgeInsets.only(
                                left: 15.0, top: 15.0, right: 15.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Most Sale Shops',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    GestureDetector(
                                      child: Text(
                                        'See All',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      onTap: () {
                                        setState(() {
                                          Utils.push(context,
                                              MostSaleData('Most Sale Shops'));
                                          //  _showToast();
                                        });
                                      },
                                    ),
                                    Icon(Icons.navigate_next)
                                  ],
                                )
                              ],
                            )),
                      )
                    ],
                  ),
                  _getList2(context),
                  SafeArea(
                    child: (Container(
                      height: 8.0,
                    )),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Padding(
                          padding:
                          EdgeInsets.only(left: 15.0, top: 15.0, right: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Recent Shops',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  GestureDetector(
                                    child: Text(
                                      'See All',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        Utils.push(
                                            context, RecentData('Recent Shops'));
                                        //_showToast();
                                      });
                                    },
                                  ),
                                  Icon(Icons.navigate_next)
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  _getList3(context),
                  SafeArea(
                    child: (Container(
                      height: 8.0,
                    )),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Padding(
                          padding:
                          EdgeInsets.only(left: 15.0, top: 15.0, right: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'High Rated Stores',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  GestureDetector(
                                    child: Text(
                                      'See All',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        Utils.push(context,
                                            HighRateData('High Rated Stores'));
                                        //_showToast();
                                      });
                                    },
                                  ),
                                  Icon(Icons.navigate_next)
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  _getCircularImgList(context),
                ],
              )
            ],
          ),
        ),
      );
  }

  Widget _getImages(BuildContext context) {
    return nearestList != null
        ? Container(
      padding: EdgeInsets.only(bottom: 15.0, left: 15.0),
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: 160,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: nearestList.length,
        itemBuilder: (context, index) {
          var nearestListData = nearestList[index];
          int ratio = nearestListData.ratio;
          List<Images> imagesList = nearestList[index].image;
          print(imagesList);
          return Container(
            padding: EdgeInsets.only(top: 10.0, right: 15.0),
            width: 95.0,
            height: 95.0,
            child: Center(
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Utils.push(
                          context,
                          ShopDetailViewNew(nearestListData.id)
                        */
/*ShopDetailView(
                                    nearestListData.image != null
                                        ? nearestListData.image[0].image
                                        : AssetImage("assets/dummy_bg.jpg"),
                                    imagesList,
                                    nearestListData.name,
                                    nearestListData.address,
                                    nearestListData.phone,
                                    nearestListData.ratio,
                                    nearestListData.email != null
                                        ? nearestListData.email
                                        : "",
                                    nearestListData.cityObj != null
                                        ? nearestListData.cityObj.title
                                        : "",
                                    nearestListData.stateObj != null
                                        ? nearestListData.stateObj.title
                                        : "")*//*
);
                    },
                    child: Container(
                      width: 80.0,
                      height: 80.0,
                      child: Center(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: 240.0,
                              height: 80.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.all(
                                    Radius.circular(10.0)),
                                image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: nearestListData.image.isNotEmpty
                                      ? NetworkImage(kBaseUserProfile +
                                      nearestListData.image[0].image)
                                      : AssetImage("assets/dummy_bg.jpg"),
                                ),
                              ),
                            ),
                            Container(
                              width: 20.0,
                              height: 15.0,
                              margin: EdgeInsets.all(4.0),
                              padding: EdgeInsets.all(1.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.green,
                                borderRadius: BorderRadius.all(
                                    Radius.circular(3.0)),
                              ),
                              alignment: Alignment.bottomRight,
                              child: Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    ratio != null ? '$ratio' : '',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0),
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: Colors.white,
                                    size: 10,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Text(
                    nearestListData.name != ""
                        ? nearestListData.name
                        : '',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
          );
        },
      ),
    )
        : Container(
      child: Text('No Data Available'),
    );
  }

  Widget _getCircularImgList(BuildContext context) {
    return highRateList != null
        ? Container(
      padding: EdgeInsets.only(bottom: 15.0, left: 15.0),
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: 160.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: highRateList.length,
        itemBuilder: (context, index) {
          var fourthListData = highRateList[index];
          return Container(
            padding: EdgeInsets.only(top: 10.0, right: 15.0),
            width: 95.0,
            height: 95.0,
            child: Center(
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Utils.pushReplacement(
                          context,
                          ShopDetailViewNew(fourthListData.id)
                        */
/* ShopDetailView(
                                    fourthListData.image != null
                                        ? fourthListData.image[0].image
                                        : AssetImage("assets/dummy_bg.jpg"),
                                    fourthListData.image,
                                    fourthListData.name,
                                    fourthListData.address,
                                    fourthListData.phone,
                                    fourthListData.ratio,
                                    fourthListData.email != null
                                        ? fourthListData.email
                                        : "",
                                    fourthListData.cityObj != null
                                        ? fourthListData.cityObj.title
                                        : "",
                                    fourthListData.stateObj != null
                                        ? fourthListData.stateObj.title
                                        : "")*//*
);
                    },
                    child: Container(
                      width: 80.0,
                      height: 80.0,
                      child: Center(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: 240.0,
                              height: 80.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: fourthListData.image.isNotEmpty
                                      ? NetworkImage(kBaseUserProfile +
                                      fourthListData.image[0].image)
                                      : AssetImage("assets/dummy_bg.jpg"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Text(
                    fourthListData.name != "" ? fourthListData.name : '',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
          );
        },
      ),
    )
        : Container(
      alignment: Alignment.center,
      child: Center(
        child: Text('No data available'),
      ),
    );
  }

  Widget _getList2(BuildContext context) {
    return mostSaleList != null
        ? Container(
      padding: EdgeInsets.only(bottom: 15.0, left: 15.0),
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: 160.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: mostSaleList.length,
        itemBuilder: (context, index) {
          var mostSaleListData = mostSaleList[index];
          int ratio = mostSaleListData.ratio;
          return Container(
            padding: EdgeInsets.only(top: 10.0, right: 15.0),
            width: 160.0,
            height: 100.0,
            child: Center(
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        Utils.pushReplacement(
                          context,ShopDetailViewNew(mostSaleListData.id)
                          */
/* ShopDetailView(
                                    mostSaleListData.image != null
                                        ? mostSaleListData.image[0].image
                                        : AssetImage("assets/dummy_bg.jpg"),
                                    mostSaleListData.image,
                                    mostSaleListData.name,
                                    mostSaleListData.address,
                                    mostSaleListData.phone,
                                    ratio,
                                    mostSaleListData.email != null
                                        ? mostSaleListData.email
                                        : "",
                                    mostSaleListData.cityObj != null
                                        ? mostSaleListData.cityObj.title
                                        : "",
                                    mostSaleListData.stateObj != null
                                        ? mostSaleListData.stateObj.title
                                        : "")*//*
,
                        );
                      });
                    },
                    child: Container(
                      width: 160.0,
                      height: 100.0,
                      child: Center(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: 160.0,
                              height: 95.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.all(
                                    Radius.circular(10.0)),
                                image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: mostSaleListData.image.isNotEmpty
                                      ? NetworkImage(kBaseUserProfile +
                                      mostSaleListData.image[0].image)
                                      : AssetImage("assets/dummy_bg.jpg"),
                                ),
                              ),
                            ),
                            Container(
                              width: 20.0,
                              height: 15.0,
                              margin: EdgeInsets.all(4.0),
                              padding: EdgeInsets.all(1.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.green,
                                borderRadius: BorderRadius.all(
                                    Radius.circular(3.0)),
                              ),
                              alignment: Alignment.bottomRight,
                              child: Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    ratio != null ? '$ratio' : '',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0),
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: Colors.white,
                                    size: 10,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Text(
                    mostSaleListData.name != null
                        ? mostSaleListData.name
                        : '',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
          );
        },
      ),
    )
        : Container(
      alignment: Alignment.center,
      child: Center(
        child: Text('No data available'),
      ),
    );
  }

  Widget _getList3(BuildContext context) {
    return recentList != null
        ? Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      child: ListView.builder(
        //padding: EdgeInsets.all(10.0),
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: recentList.length,
        itemBuilder: (context, index) {
          var newestListData = recentList[index];
          int ratio = newestListData.ratio;
          return GestureDetector(
            onTap: () {
              Utils.pushReplacement(
                context,
                ShopDetailViewNew(newestListData.id)
                */
/* ShopDetailView(
                          newestListData.image != null
                              ? newestListData.image[0].image
                              : AssetImage("assets/dummy_bg.jpg"),
                          newestListData.image.length != 0
                              ? newestListData.image
                              : AssetImage("assets/dummy_bg.jpg"),
                          newestListData.name,
                          newestListData.address,
                          newestListData.phone,
                          ratio,
                          newestListData.email != null
                              ? newestListData.email
                              : "",
                          newestListData.cityObj != null
                              ? newestListData.cityObj.title
                              : "",
                          newestListData.stateObj != null
                              ? newestListData.stateObj.title
                              : "")*//*
,
              );
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 100.0,
              padding: EdgeInsets.only(left: 15.0),
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 15.0, left: 15.0),
                    width: 100.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius:
                      BorderRadius.all(Radius.circular(10.0)),
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: newestListData.image.isNotEmpty
                            ? NetworkImage(kBaseUserProfile +
                            newestListData.image[0].image)
                            : AssetImage("assets/dummy_bg.jpg"),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 115,
                    child: ListTile(
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: RichText(
                              textAlign: TextAlign.start,
                              text: new TextSpan(
                                style: DefaultTextStyle.of(context).style,
                                children: <TextSpan>[
                                  TextSpan(
                                      text: newestListData.name != null
                                          ? newestListData.name
                                          : '',
                                      style: new TextStyle(
                                          fontSize: 16.0,
                                          color: Colors.black)),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      subtitle: Row(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: RichText(
                              text: new TextSpan(
                                style: DefaultTextStyle.of(context).style,
                                children: <TextSpan>[
                                  TextSpan(
                                      text: newestListData.address != null
                                          ? newestListData.address
                                          : '',
                                      style: new TextStyle(
                                          fontSize: 13.0,
                                          color: Colors.grey)),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      // Todo jatin here
                      trailing: Container(
                        width: 20.0,
                        height: 15.0,
                        margin: EdgeInsets.only(top: 6.0),
                        padding: EdgeInsets.all(1.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.green,
                          borderRadius:
                          BorderRadius.all(Radius.circular(3.0)),
                        ),
                        alignment: Alignment.bottomRight,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              ratio != null ? '$ratio' : '5',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 12.0),
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.white,
                              size: 10,
                            ),
                          ],
                        ),
                      ),
                      // isThreeLine: true,
                    ),
                  ),
                ],
              ),
            ),
          );
          */
/*return GestureDetector(
                  onTap: () {
                    Utils.push(
                        context,
                        *//*
 */
/*ShopDetailView(newestListData != null
                            ? newestListData
                            : "")*//*
 */
/*
                        ShopDetailView(
                            newestListData.image != null
                                ? newestListData.image[0].image
                                : '',
                            *//*
 */
/*   newestListData.image != null
                                ? newestListData.image
                                : '',*//*
 */
/*
                            newestListData.name,
                            newestListData.address,
                            newestListData.phone,
                            ratio,
                            newestListData.email != null
                                ? newestListData.email
                                : "",
                            newestListData.cityObj != null
                                ? newestListData.cityObj.title
                                : "",
                            newestListData.stateObj != null
                                ? newestListData.stateObj.title
                                : ""));
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 100.0,
                    padding: EdgeInsets.only(left: 15.0),
                    color: Colors.white,
                    child: Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 15.0, left: 15.0),
                          width: 100.0,
                          height: 80.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: newestListData.image != null
                                  ? NetworkImage(kBaseUserProfile +
                                      newestListData.image[0].image)
                                  : AssetImage("assets/dummy_bg.jpg"),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 115,
                          child: ListTile(
                            title: Row(
                              //mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: RichText(
                                    textAlign: TextAlign.start,
                                    text: new TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: newestListData.name != null
                                                ? newestListData.name
                                                : '',
                                            style: new TextStyle(
                                                fontSize: 16.0,
                                                color: Colors.black)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            subtitle: Row(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: RichText(
                                    text: new TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: newestListData.address != null
                                                ? newestListData.address
                                                : 'ABC Street,',
                                            style: new TextStyle(
                                                fontSize: 13.0,
                                                color: Colors.grey)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            trailing: Container(
                              width: 20.0,
                              height: 15.0,
                              margin: EdgeInsets.only(top: 6.0),
                              padding: EdgeInsets.all(1.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3.0)),
                              ),
                              alignment: Alignment.bottomRight,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    ratio != null ? '$ratio' : '',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 12.0),
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: Colors.white,
                                    size: 10,
                                  ),
                                ],
                              ),
                            ),
                            // isThreeLine: true,
                          ),
                        ),
                      ],
                    ),
                  ),
                );*//*

        },
      ),
    )
        : Container(
      alignment: Alignment.center,
      child: Center(
        child: Text('No data available'),
      ),
    );
  }
}
*/
