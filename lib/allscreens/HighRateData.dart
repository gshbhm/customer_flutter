import 'package:flutter/material.dart';
import 'package:shopkeeperapp/Bloc/models/MostSaleResponse.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailView.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailViewNew.dart';
import 'package:shopkeeperapp/constants/CustomLoader.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';

class HighRateData extends StatefulWidget {
  final String title;

  HighRateData(this.title);

  @override
  _HighRateDataState createState() => _HighRateDataState();
}

MostSaleResponse mostSaleResponse = new MostSaleResponse();

List<Data> highRateList = [];
int pageNo = 0;

class _HighRateDataState extends State<HighRateData> {
  CustomLoader _customLoader = CustomLoader();

  _nearestShopsApi() async {
    String body = '?' +
        kSkip +
        '=' +
        '$pageNo' +
        '&' +
        kTake +
        '=' +
        '6' +
        '&' +
        kLat +
        '=' +
        '29.685629' +
        '&' +
        kLong +
        '=' +
        '76.990547';
    print(body);
    // _customLoader.showLoader(context);

    var response = await APIServices.getMethod(context, kHighRated, body);
    print(response);
    if (response['data'] != null) {
      setState(() {
        List<dynamic> listData = response['data'];
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = Data.fromJson(dict);
          highRateList.add(model);
          //   _customLoader.hideLoader();
        }
      });
    } else {
      //   _customLoader.hideLoader();

      Utils.showErrorAlert('Error', response['message'], context);
    }
  }

  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
        () => 'Data Loaded',
  );

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: new MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              color: Colors.white,
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
            backgroundColor: Colors.purple,
            centerTitle: true,
            title: Text(
              widget.title,
              style: TextStyle(color: Colors.white),
            ),
          ),
          body: FutureBuilder<String>(
              future: _calculation,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView(
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      Column(
                        children: <Widget>[_getList3(context)],
                      )
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Center(
                      child: Text("${snapshot.error}",
                          style: Theme
                              .of(context)
                              .textTheme
                              .headline));
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              }),
        ),
      ),
    );
  }

  Widget _getList3(BuildContext context) {
    return highRateList != null
        ? Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: MediaQuery
          .of(context)
          .size
          .height,
      child: ListView.builder(
        //padding: EdgeInsets.all(10.0),
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: highRateList.length,

        // itemExtent: 100,
        itemBuilder: (context, index) {
          var highRateListData = highRateList[index];
          int ratio = highRateListData.ratio;
          return GestureDetector(
              onTap: () {
                Utils.push(
                  context,
                  ShopDetailViewNew(highRateListData.id)
                  /*ShopDetailView(
                            highRateListData.image != null
                                ? highRateListData.image[0].image
                                : AssetImage("assets/dummy_bg.jpg"),
                               highRateListData.image != null
                                  ? highRateListData.image
                                  : '',
                            highRateListData.name,
                            highRateListData.address,
                            highRateListData.phone,
                            ratio,
                            highRateListData.email != null
                                ? highRateListData.email
                                : "",
                            highRateListData.cityObj != null
                                ? highRateListData.cityObj.title
                                : "",
                            highRateListData.stateObj != null
                                ? highRateListData.stateObj.title
                                : "")*/
                  ,
                );
              },
              child: Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                height: 100.0,
                padding: EdgeInsets.only(left: 15.0),
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 15.0, left: 15.0),
                      width: 100.0,
                      height: 80.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius:
                        BorderRadius.all(Radius.circular(10.0)),
                        image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: highRateListData.image.isNotEmpty
                              ? NetworkImage(kBaseUserProfile +
                              highRateListData.image[1].image)
                              : AssetImage("assets/dummy_bg.jpg"),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width - 115,
                      child: ListTile(
                        title: Row(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: RichText(
                                textAlign: TextAlign.start,
                                text: new TextSpan(
                                  style:
                                  DefaultTextStyle
                                      .of(context)
                                      .style,
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                        highRateListData.name != null
                                            ? highRateListData.name
                                            : '',
                                        style: new TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.black)),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),

                        subtitle: Row(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: RichText(
                                text: new TextSpan(
                                  style:
                                  DefaultTextStyle
                                      .of(context)
                                      .style,
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: highRateListData.address !=
                                            null
                                            ? highRateListData.address
                                            : '',
                                        style: new TextStyle(
                                            fontSize: 13.0,
                                            color: Colors.grey)),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        // Todo jatin here
                        trailing: Container(
                          width: 20.0,
                          height: 15.0,
                          margin: EdgeInsets.only(top: 6.0),
                          padding: EdgeInsets.all(1.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.green,
                            borderRadius:
                            BorderRadius.all(Radius.circular(3.0)),
                          ),
                          alignment: Alignment.bottomRight,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                ratio != null ? '$ratio' : '5',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12.0),
                              ),
                              Icon(
                                Icons.star,
                                color: Colors.white,
                                size: 10,
                              ),
                            ],
                          ),
                        ),
                        // isThreeLine: true,
                      ),
                    ),
                  ],
                ),
              ));
        },
      ),
    )
        : Container(
      alignment: Alignment.center,
      child: Text('No Data Available'),
    );
  }

  @override
  void initState() {
    super.initState();
    _nearestShopsApi();
  }

  @override
  void dispose() {
    super.dispose();
    highRateList.clear();
  }
}
