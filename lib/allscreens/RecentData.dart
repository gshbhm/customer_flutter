import 'package:flutter/material.dart';
import 'package:shopkeeperapp/Bloc/models/MostSaleResponse.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailView.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailViewNew.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';

class RecentData extends StatefulWidget {
  final String title;

  RecentData(this.title);

  @override
  _RecentDataState createState() => _RecentDataState();
}

MostSaleResponse mostSaleResponse = new MostSaleResponse();

List<Data> recentList = [];
int pageNo = 0;

class _RecentDataState extends State<RecentData> {
  _recentShopsApi() async {
    String body = '?' +
        kSkip +
        '=' +
        '$pageNo' +
        '&' +
        kTake +
        '=' +
        '2' +
        '&' +
        kLat +
        '=' +
        '29.685629' +
        '&' +
        kLong +
        '=' +
        '76.990547';
    print(body);
    var response = await APIServices.getMethod(context, kRecent, body);
    print(response);
    if (response['data'] != null) {
      setState(() {
        List<dynamic> listData = response['data'];
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = Data.fromJson(dict);
          recentList.add(model);
        }
      });
    } else {
      Utils.showErrorAlert('Error', response['message'], context);
    }
  }
  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
        () => 'Data Loaded',
  );
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: new MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              color: Colors.white,
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
            backgroundColor: Colors.purple,
            centerTitle: true,
            title: Text(
              widget.title,
              style: TextStyle(color: Colors.white),
            ),
          ),
          body: FutureBuilder<String>(
              future: _calculation,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView(
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      Column(
                        children: <Widget>[_getList3(context)],
                      )
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Center(
                      child: Text("${snapshot.error}",
                          style: Theme.of(context).textTheme.headline));
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              }),
        ),
      ),
    );
  }

  Widget _getList3(BuildContext context) {
    return recentList != null
        ? Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: recentList.length,
              itemBuilder: (context, index) {
                var mostSaleList = recentList[index];
                int ratio = mostSaleList.ratio;
                print(recentList.length);
                return GestureDetector(
                  onTap: () {
                    Utils.pushReplacement(
                      context,
                      ShopDetailViewNew(mostSaleList.id)
                      /*ShopDetailView(
                          mostSaleList.image != null
                              ? mostSaleList.image[0].image
                              : AssetImage("assets/dummy_bg.jpg"),
                            mostSaleList.image != null
                                ? mostSaleList.image
                                : '',
                          mostSaleList.name,
                          mostSaleList.address,
                          mostSaleList.phone,
                          ratio,
                          mostSaleList.email != null ? mostSaleList.email : "",
                          mostSaleList.cityObj != null
                              ? mostSaleList.cityObj.title
                              : "",
                          mostSaleList.stateObj != null
                              ? mostSaleList.stateObj.title
                              : "")*/
                      ,
                    );
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 100.0,
                    padding: EdgeInsets.only(left: 15.0),
                    color: Colors.white,
                    child: Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 15.0, left: 15.0),
                          width: 100.0,
                          height: 80.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: mostSaleList.image.isNotEmpty
                                  ? NetworkImage(kBaseUserProfile +
                                      mostSaleList.image[0].image)
                                  : AssetImage("assets/dummy_bg.jpg"),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 115,
                          child: ListTile(
                            title: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: RichText(
                                    textAlign: TextAlign.start,
                                    text: new TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: mostSaleList.name != null
                                                ? mostSaleList.name
                                                : '',
                                            style: new TextStyle(
                                                fontSize: 16.0,
                                                color: Colors.black)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            subtitle: Row(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: RichText(
                                    text: new TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: mostSaleList.address != null
                                                ? mostSaleList.address
                                                : '',
                                            style: new TextStyle(
                                                fontSize: 13.0,
                                                color: Colors.grey)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            // Todo jatin here
                            trailing: Container(
                              width: 20.0,
                              height: 15.0,
                              margin: EdgeInsets.only(top: 6.0),
                              padding: EdgeInsets.all(1.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3.0)),
                              ),
                              alignment: Alignment.bottomRight,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    ratio != null ? '$ratio' : '5',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 12.0),
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: Colors.white,
                                    size: 10,
                                  ),
                                ],
                              ),
                            ),
                            // isThreeLine: true,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          )
        : Container(
            alignment: Alignment.center,
            child: Text('No Data Available'),
          );
  }

  @override
  void initState() {
    super.initState();
    _recentShopsApi();
  }

  @override
  void dispose() {
    super.dispose();
    recentList.clear();
  }
}
