/*
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shopkeeperapp/Bloc/models/MostSaleResponse.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:shopkeeperapp/webservices/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class ShopDetailView extends StatefulWidget {
  final String name, address, phone, image, city, state, email;
  final int ratio;

  List<Image> imageList = [];

  ShopDetailView(
    this.image,
    this.imageList,
    this.name,
    this.address,
    this.phone,
    this.ratio,
    this.email,
    this.city,
    this.state,
  );

  @override
  _ShopDetailViewState createState() => _ShopDetailViewState();
}

class _ShopDetailViewState extends State<ShopDetailView> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        leading: GestureDetector(
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onTap: () {
            setState(() {
              Navigator.pop(context);
            });
          },
        ),
        title: Text(
          "Shopkeeper App",
          style: TextStyle(color: Colors.white),
          textAlign: TextAlign.center,
        ),
        backgroundColorStart: ColorConstants.kGradientTop,
        backgroundColorEnd: ColorConstants.kGradientBottom,
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Container(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 180.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: widget.imageList.length != 0
                            ? NetworkImage(
                                kBaseUserProfile + widget.imageList[0].image)
                            : AssetImage("assets/dummy_bg.jpg"),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text(
                      widget.name,
                      style: TextStyle(
                          fontSize: 24.0, fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      widget.address,
                      style: TextStyle(fontSize: 14.0, color: AppColors.kGrey),
                    ),
                    trailing: Container(
                        width: 55.0,
                        height: 60.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                        alignment: Alignment.center,
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: 55.0,
                              height: 20.0,
                              padding: EdgeInsets.only(left: 2.0, right: 2.0),
                              alignment: Alignment.center,
                              color: Colors.lightGreen,
                              child: Text(
                                '${widget.ratio}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14.0),
                              ),
                            ),
                            Container(
                              width: 55.0,
                              height: 30.0,
                              padding: EdgeInsets.only(left: 2.0, right: 2.0),
                              alignment: Alignment.center,
                              color: Colors.grey.shade400,
                              child: Text(
                                'Rating',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14.0),
                              ),
                            ),
                          ],
                        )),
                  ),
                  Container(
                    // height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),

                    child: GestureDetector(
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.call),
                          SizedBox(
                            width: 2.0,
                          ),
                          Text(
                            widget.phone,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: 14.0,
                            ),
                          ),
                        ],
                      ),
                      onTap: () {
                        setState(() {
                          var phone = widget.phone;
                          launch("tel://$phone");
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width - 30,
                    height: 1.0,
                    child: Container(
                      color: Colors.grey.shade300,
                    ),
                  ),
                  Container(
                    // height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.email),
                        SizedBox(
                          width: 2.0,
                        ),
                        Text(
                          widget.email,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.kGrey,
                            fontSize: 14.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width - 30,
                    height: 1.0,
                    child: Container(
                      color: Colors.grey.shade300,
                    ),
                  ),
                  Container(
                    // height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'City ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ) */
/*Icon(Icons.location_city)*//*
,
                        SizedBox(
                          width: 2.0,
                        ),
                        Text(
                          widget.city,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.kGrey,
                            fontSize: 14.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // height: MediaQuery.of(context).size.height,
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'State ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          width: 2.0,
                        ),
                        Text(
                          widget.state,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.kGrey,
                            fontSize: 14.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width - 30,
                    height: 1.0,
                    child: Container(
                      color: Colors.grey.shade300,
                    ),
                  ),
                  Container(
                    // height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.all(10.0),
                    child: Column(children: <Widget>[
                      Text(
                        'More Images',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.purple,
                          fontSize: 18.0,
                        ),
                      ),
                    ]),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  */
/*Container(
                          // height: 200,
                          width: MediaQuery.of(context).size.width,
                          child: GridView.count(
                            primary: true,
                            crossAxisCount: 2,
                            childAspectRatio: 0.80,
                            children: List.generate(
                              _imageslist.length,
                              (index) {
                                return Card(
                                  elevation: 1.5,
                                  child: new Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    mainAxisSize: MainAxisSize.min,
                                    verticalDirection: VerticalDirection.down,
                                    children: <Widget>[
                                      Image.asset(_imageslist[index]),
                                      // AssetImage(imageslist);
                                    ],
                                  ),
                                );
                              },
                            ),
                          ))*//*

                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
*/
