import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:shopkeeperapp/Bloc/models/MostSaleResponse.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailView.dart';
import 'package:shopkeeperapp/allscreens/ShopDetailViewNew.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  var searchController = TextEditingController();
  List<Data> searchList = [];
  bool _isEdit = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    searchList.clear();
  }

  _searchShopsApi() async {
    String body = '?' +
        kSkip +
        '=' +
        '0' +
        '&' +
        kTake +
        '=' +
        '4' +
        '&' +
        kSearch +
        '=' +
        searchController.text;
    print(body);

    var response = await APIServices.getMethod(context, kSearch, body);
    print(response);
    if (response['data'] != null) {
      setState(() {
        List<dynamic> listData = response['data'];
        for (int i = 0; i < listData.length; i++) {
          Map<String, dynamic> dict = listData[i];
          print(dict);
          var model = Data.fromJson(dict);
          searchList.add(model);
        }
      });
    }
  }
 /* Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
        () => 'Data Loaded',
  );*/
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        title: Text(
          "Shopkeeper App",
          style: TextStyle(color: Colors.white),
          textAlign: TextAlign.center,
        ),
        bottom: new PreferredSize(
            child: Container(
              color: Colors.grey.shade200,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(
                  left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8.0),
                ),
                width: MediaQuery.of(context).size.width - 40,
                child: Row(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: TextFormField(
                          controller: searchController,
                          autovalidate: true,
                          onFieldSubmitted: (String str) {
                            setState(() {
                              if (searchController.text.isEmpty) {
                                searchList.clear();
                              } else {
                                searchList.clear();
                                _searchShopsApi();
                              }
                            });
                          },
                          onChanged: (String arg) {
                            print(arg);
                            print(arg.length);
                            if (arg.length == 0) {
                              setState(() {
                                searchList.clear();
                                _isEdit = false;
                              });
                            } else if (arg.length != 0) {
                              setState(() {
                                _isEdit = true;
                              });
                            }
                          },
                          decoration: InputDecoration(
                            prefixIcon:
                                Icon(Icons.search, color: Colors.grey.shade500),
                            hintStyle: TextStyle(
                                fontSize: 17.0, color: Colors.grey.shade500),
                            hintText: 'Search',
                            //"Username or email address",
                            border: InputBorder.none,
                            suffixIcon: GestureDetector(
                              child: Visibility(
                                visible: _isEdit,
                                child: Icon(Icons.cancel,
                                    color: Colors.grey.shade500),
                              ),
                              onTap: () {
                                setState(() {
                                  searchController.clear();
                                  searchList.clear();
                                  _isEdit = false;
                                });
                              },
                            ),
                          ),
                          style: TextStyle(
                            color: ColorConstants.mainColor1,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: new Size(MediaQuery.of(context).size.width, 60.0)),
        backgroundColorStart: ColorConstants.kGradientTop,
        backgroundColorEnd: ColorConstants.kGradientBottom,
      ),
      body:ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Column(
            children: <Widget>[_getList3(context)],
          )
        ],
      ),/*FutureBuilder<String>(
          future: _calculation,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  Column(
                    children: <Widget>[_getList3(context)],
                  )
                ],
              );
            } else if (snapshot.hasError) {
              return Center(
                  child: Text("${snapshot.error}",
                      style: Theme.of(context).textTheme.headline));
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),*/
    );
  }

  Widget _getList3(BuildContext context) {
    return searchList != null
        ? Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height - 200,
            child: ListView.builder(
              //padding: EdgeInsets.all(10.0),
              scrollDirection: Axis.vertical,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: searchList.length,
              // itemExtent: 100,
              itemBuilder: (context, index) {
                var mostSaleListData = searchList[index];
                int ratio = mostSaleListData.ratio;
                return GestureDetector(
                    onTap: () {
                      Utils.push(
                          context,
                          ShopDetailViewNew(mostSaleListData.id))
                          /*ShopDetailView(
                              mostSaleListData.image != null
                                  ? mostSaleListData.image[0].image
                                  : '',
                                mostSaleListData.image != null
                                  ? mostSaleListData.image
                                  : '',
                              mostSaleListData.name,
                              mostSaleListData.address,
                              mostSaleListData.phone,
                              ratio,
                              mostSaleListData.email != null
                                  ? mostSaleListData.email
                                  : "",
                              mostSaleListData.cityObj != null
                                  ? mostSaleListData.cityObj.title
                                  : "",
                              mostSaleListData.stateObj != null
                                  ? mostSaleListData.stateObj.title
                                  : ""))*/;
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 100.0,
                      padding: EdgeInsets.only(left: 15.0),
                      color: Colors.white,
                      child: Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 15.0, left: 15.0),
                            width: 100.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              image: new DecorationImage(
                                fit: BoxFit.fill,
                                image: mostSaleListData.image.isNotEmpty
                                    ? NetworkImage(kBaseUserProfile +
                                        mostSaleListData.image[0].image)
                                    : AssetImage("assets/dummy_bg.jpg"),
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 115,
                            child: ListTile(
                              title: Row(
                                //mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: RichText(
                                      textAlign: TextAlign.start,
                                      text: new TextSpan(
                                        style:
                                            DefaultTextStyle.of(context).style,
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  mostSaleListData.name != null
                                                      ? mostSaleListData.name
                                                      : '',
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colors.black)),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              subtitle: Row(
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: RichText(
                                      text: new TextSpan(
                                        style:
                                            DefaultTextStyle.of(context).style,
                                        children: <TextSpan>[
                                          TextSpan(
                                              text: mostSaleListData.address !=
                                                      null
                                                  ? mostSaleListData.address
                                                  : '',
                                              style: new TextStyle(
                                                  fontSize: 13.0,
                                                  color: Colors.grey)),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              // Todo jatin here
                              trailing: Container(
                                width: 20.0,
                                height: 15.0,
                                margin: EdgeInsets.only(top: 6.0),
                                padding: EdgeInsets.all(1.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: Colors.green,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3.0)),
                                ),
                                alignment: Alignment.bottomRight,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      ratio != null ? '$ratio' : '5',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12.0),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                  ],
                                ),
                              ),
                              // isThreeLine: true,
                            ),
                          ),
                        ],
                      ),
                    ));
              },
            ),
          )
        : Container(
            alignment: Alignment.center,
            child: Text('No Data Available'),
          );
  }
}
