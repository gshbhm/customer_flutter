import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shopkeeperapp/Bloc/models/PointsResponse.dart';
import 'package:shopkeeperapp/allscreens/CreateSale.dart';
import 'package:shopkeeperapp/allscreens/HomeScreen.dart';
import 'package:shopkeeperapp/allscreens/PaymentList.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/constants/memory_management.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  PointsResponse pointsResponse;
  var data;

  @override
  void initState() {
    super.initState();
    _pointsApi();
  }

  @override
  void dispose() {
    super.dispose();
    _pointsApi();
    return null;
  }

  _pointsApi() async {
    String body = '?' + kUserToken + '=' + MemoryManagement.getAccessToken();
    print(body);

    var response = await APIServices.getMethod(context, kPoints, body);
    print(response);
    if (response != null) {
      setState(() {
        data = response['data'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return /*MaterialApp(
      debugShowCheckedModeBanner: false,
      home:*/
        Scaffold(
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 200.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [
                        0.1,
                        0.4,
                        0.9
                      ],
                      colors: [
                        ColorConstants.kGradientTop,
                        ColorConstants.kGradientCenter,
                        ColorConstants.kGradientBottom,
                      ]),
                  shape: BoxShape.rectangle,
                ),
                padding: EdgeInsets.fromLTRB(28.0, 28.0, 28.0, 28.0),
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Text(
                        data != null ? '$data' : '6.58',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.none),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 36.0,
                            width: 36.0,
                            child: Image.asset('assets/reward.png'),
                          ),
                          Text(
                            'Points earned',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                /*fontWeight: FontWeight.bold,*/
                                decoration: TextDecoration.none),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 8.0,
          ),
          Container(
            padding: EdgeInsets.fromLTRB(15.0, 18.0, 15.0, 18.0),
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  MemoryManagement.getName() != null
                      ? MemoryManagement.getName()
                      : "a11",
                  style: TextStyle(
                      color: Colors.grey.shade800,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none),
                ),
                SizedBox(
                  height: 6.0,
                ),
                Text(
                  MemoryManagement.getEmail() != null
                      ? MemoryManagement.getEmail()
                      : "a11@gmail.com",
                  style: TextStyle(
                      color: Colors.grey.shade400,
                      fontSize: 13.0,
                      decoration: TextDecoration.none),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 2.0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              color: Colors.grey.shade200,
            ),
          ),
          SizedBox(
            height: 8.0,
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                Navigator.push(
                  context,
                  new CupertinoPageRoute(builder: (context) => PaymentList()),
                );
              });
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(15.0, 18.0, 15.0, 18.0),
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Transactions',
                    style: TextStyle(
                        color: Colors.grey.shade800,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none),
                  ),
                  Icon(
                    Icons.navigate_next,
                    color: Colors.grey.shade800,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 8.0,
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                Navigator.push(
                  context,
                  new CupertinoPageRoute(builder: (context) => CreateSales()),
                );
              });
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(15.0, 18.0, 15.0, 18.0),
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Purchase',
                    style: TextStyle(
                        color: Colors.grey.shade800,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none),
                  ),
                  Icon(
                    Icons.navigate_next,
                    color: Colors.grey.shade800,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 8.0,
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                MemoryManagement.clearMemory();
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => new HomeScreen(true,
                        double.parse('29.685629'), double.parse('76.990547'))));
                /* Utils.showOptionDialog('LOGOUT',
                    'Are you sure you want to logout?', context,onTap);*/
              });
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(15.0, 18.0, 15.0, 18.0),
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Logout',
                    style: TextStyle(
                        color: Colors.grey.shade500,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none),
                  ),
                  Icon(
                    Icons.power_settings_new,
                    color: Colors.grey.shade500,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      //  ),
    );
  }
}
