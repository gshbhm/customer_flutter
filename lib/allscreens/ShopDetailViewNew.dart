import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:shopkeeperapp/Bloc/models/ShopDetailViewReponse.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:shopkeeperapp/constants/CustomLoader.dart';
import 'package:shopkeeperapp/constants/ReusableWidgets.dart';
import 'package:shopkeeperapp/webservices/Services.dart';
import 'package:shopkeeperapp/webservices/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class ShopDetailViewNew extends StatefulWidget {
  final int id;

  ShopDetailViewNew(
    this.id,
  );

  @override
  _ShopDetailViewNewState createState() => _ShopDetailViewNewState();
}

CustomLoader _customLoader = CustomLoader();
String shopImage;

class _ShopDetailViewNewState extends State<ShopDetailViewNew> {
  Data shopDetailViewResponse;

  @override
  void initState() {
    super.initState();
    _shopDetailApi();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _shopDetailApi() async {
    String body = '${widget.id}';
    //  print(body);
    //  _customLoader.showLoader(context);
    var response = await APIServices.getMethod(context, kShopDetailView, body);
    print(response);
    // _customLoader.hideLoader();

    if (response["status"] == 200) {
      setState(() {
        //  _customLoader.hideLoader();
        var model = response["data"];
        shopDetailViewResponse = new Data.fromJson(model);
      });
    } else if (response["status"] == 422) {
      //  _customLoader.hideLoader();
      getToast(msg: "Error Occur");
    }
  }

  Future<String> _calculation = Future<String>.delayed(
    Duration(seconds: 2),
    () => 'Data Loaded',
  );

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: GradientAppBar(
          leading: GestureDetector(
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onTap: () {
              setState(() {
                Navigator.pop(context);
              });
            },
          ),
          title: Text(
            "Shopkeeper App",
            style: TextStyle(color: Colors.white),
            textAlign: TextAlign.center,
          ),
          backgroundColorStart: ColorConstants.kGradientTop,
          backgroundColorEnd: ColorConstants.kGradientBottom,
        ),
        body: FutureBuilder<String>(
            future: _calculation,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView(
                  scrollDirection: Axis.vertical,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 180.0,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: shopDetailViewResponse.image != null &&
                                      shopDetailViewResponse
                                          .image.isNotEmpty
                                  ? NetworkImage(kBaseUserProfile +
                                      shopDetailViewResponse.image[0].image)
                                  : AssetImage("assets/dummy_bg.jpg"),
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            shopDetailViewResponse.name != null
                                ? shopDetailViewResponse.name
                                : "",
                            style: TextStyle(
                                fontSize: 24.0,
                                fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text(
                            shopDetailViewResponse.address != null
                                ? shopDetailViewResponse.address
                                : "",
                            style: TextStyle(
                                fontSize: 14.0, color: AppColors.kGrey),
                          ),
                          trailing: Container(
                              width: 55.0,
                              height: 60.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0)),
                              ),
                              alignment: Alignment.center,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    width: 55.0,
                                    height: 20.0,
                                    padding: EdgeInsets.only(
                                        left: 2.0, right: 2.0),
                                    alignment: Alignment.center,
                                    color: Colors.lightGreen,
                                    child: Text(
                                      shopDetailViewResponse.ratio != null
                                          ? "${shopDetailViewResponse.ratio}"
                                          : "",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14.0),
                                    ),
                                  ),
                                  Container(
                                    width: 55.0,
                                    height: 30.0,
                                    padding: EdgeInsets.only(
                                        left: 2.0, right: 2.0),
                                    alignment: Alignment.center,
                                    color: Colors.grey.shade400,
                                    child: Text(
                                      'Rating',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14.0),
                                    ),
                                  ),
                                ],
                              )),
                        ),
                        Container(
                          // height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10.0),

                          child: GestureDetector(
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.call),
                                SizedBox(
                                  width: 2.0,
                                ),
                                Text(
                                  shopDetailViewResponse.phone != null
                                      ? "${shopDetailViewResponse.phone}"
                                      : "",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 14.0,
                                  ),
                                ),
                              ],
                            ),
                            onTap: () {
                              setState(() {
                                var phone =
                                    shopDetailViewResponse.phone != null
                                        ? "${shopDetailViewResponse.phone}"
                                        : "";
                                launch("tel://$phone");
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 30,
                          height: 1.0,
                          child: Container(
                            color: Colors.grey.shade300,
                          ),
                        ),
                        Container(
                          // height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.email),
                              SizedBox(
                                width: 2.0,
                              ),
                              Text(
                                shopDetailViewResponse.email != null
                                    ? shopDetailViewResponse.email
                                    : "",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.kGrey,
                                  fontSize: 14.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 30,
                          height: 1.0,
                          child: Container(
                            color: Colors.grey.shade300,
                          ),
                        ),
                        Container(
                          // height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            children: <Widget>[
                              Text(
                                'City ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              /*Icon(Icons.location_city),*/
                              SizedBox(
                                width: 2.0,
                              ),
                              Text(
                                shopDetailViewResponse.cityObj.title != null
                                    ? shopDetailViewResponse.cityObj.title
                                    : "",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.kGrey,
                                  fontSize: 14.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          // height: MediaQuery.of(context).size.height,
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            children: <Widget>[
                              Text(
                                'State ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 2.0,
                              ),
                              Text(
                                shopDetailViewResponse.stateObj.title !=
                                        null
                                    ? shopDetailViewResponse.stateObj.title
                                    : "",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.kGrey,
                                  fontSize: 14.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 30,
                          height: 1.0,
                          child: Container(
                            color: Colors.grey.shade300,
                          ),
                        ),
                        getSpacer(height: 10,width: 0),
                        Row(
                          children: <Widget>[
                            getSpacer(height: 0,width: 10),
                            Text(
                              'More Images',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Colors.purple,
                                fontSize: 18.0,
                              ),
                            ),
                          ],
                        ),
                        getSpacer(height: 10,width: 0),
                        Container(
                          child: GridView.builder(
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                            ),
                            itemCount: shopDetailViewResponse.image.length,
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                height: 80.0,
                                width: 80.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: shopDetailViewResponse.image !=
                                                null &&
                                            shopDetailViewResponse
                                                .image.isNotEmpty
                                        ? NetworkImage(kBaseUserProfile +
                                            shopDetailViewResponse
                                                .image[index].image)
                                        : AssetImage(
                                            "assets/dummy_bg.jpg"),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                return Center(
                    child: Text("${snapshot.error}",
                        style: Theme.of(context).textTheme.headline));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }
}
