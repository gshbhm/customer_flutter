import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:toast/toast.dart';

class Utils {
  static List getCountriesList() {
    List<String> _listViewData = [
      "German",
      "English",
      "French",
      "Italian",
      "Spanish",
      "Russian",
      "Polish",
      "Portuguese",
      "Turkish",
      "Norwegian",
      "Danish",
      "Swedish",
      "Dutch",
      "Hungarian",
      "Ukrainian",
      "Czech"
    ];
  }

  static onLoading(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          child: SpinKitCubeGrid(
            color: Colors.purple,
            size: 50.0,
          ),
        );
      },
    );
  }

  static void showOptionDialog(title, body, context, VoidCallback onTap) {
    Alert(
      style: AlertStyle(
        animationType: AnimationType.grow,
      ),
      context: context,
      type: AlertType.none,

      // image:Image.asset('assets/exit.png',scale: 0.0,),
      title: title,
      desc: body,
      buttons: [
        DialogButton(
          child: Text(
            "Cancel",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.grey,
        ),
        DialogButton(
          child: Text(
            "Yes",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            Navigator.pop(context);
            /* MemoryManagement.clearMemory();
            // MemoryManagement.setLogin(isLogin: false);
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(true,
                    double.parse('29.685629'), double.parse('76.990547'))));*/
          },
          color: ColorConstants.mainColor1,
        ),
      ],
    ).show();
  }

  static showAlertDialog({context, title, message, iconData}) {
    return showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: iconData == null ? Text("$title") : Icon(iconData),
            content: Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text("$message"),
            ),
            actions: <Widget>[
              CupertinoButton(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: Text(
                  "Ok",
                  style: TextStyle(color: AppColors.kGreen),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  static void showToast(context, message) {
    Toast.show(message, context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }

  static void showErrorAlert(title, body, context) {
    Alert(
      style: AlertStyle(
        animationType: AnimationType.grow,
      ),
      context: context,
      type: AlertType.error,
      title: title,
      desc: body,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.purple,
        ),
      ],
    ).show();
  }

  //Check for internet Connection

  static Future<bool> isConnectedToInternet() async {
    try {
      final result = await InternetAddress.lookup('google.com')
          .timeout(const Duration(seconds: 5));
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } catch (_) {
      return false;
    }
  }

  static void push(context, dartName) {
    Navigator.push(
      context,
      new CupertinoPageRoute(builder: (context) => dartName),
    );
  }

  static void pushReplacement(context, dartName) {
    Navigator.push(
      context,
      new CupertinoPageRoute(builder: (context) => dartName),
    );
  }

  static void pushAndRemoveUntil(context, dartName) {
    Navigator.pushAndRemoveUntil(
        context,
        CupertinoPageRoute(builder: (context) => dartName),
        (Route<dynamic> route) => false);
  }

  static Widget getPlaceholderText([String text = "Work in progress..."]) {
    return Center(
      child: Text(
        text,
        style: TextStyle(fontSize: 25, color: Colors.purple),
      ),
    );
  }
}
/*
  static void showInProgressAlert(title, body, context) {
    Alert(
      style: AlertStyle(
        animationType: AnimationType.grow,
      ),
      context: context,
//      type: AlertType.info,
      title: title,
      desc: body,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Palette.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: prefix0.ColorConstants.mainColor1,
        ),
      ],
    ).show();
  }

  static void showToast(context,message){
    Toast.show(message, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
  }

  static void showOptionDialog(title, body, context, VoidCallback onTap) {
    Alert(
      style: AlertStyle(
        animationType: AnimationType.grow,
      ),
      context: context,
      type: AlertType.none,
      title: title,
      desc: body,
      buttons: [
        DialogButton(
          child: Text(
            "Cancel",
            style: TextStyle(color: Palette.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: prefix0.ColorConstants.mainColor1,
        ),
        DialogButton(
          child: Text(
            "Yes",
            style: TextStyle(color: Palette.white, fontSize: 20),
          ),
          onPressed: () {
            onTap();
          },
          color: prefix0.ColorConstants.mainColor1,
        ),
      ],
    ).show();
  }


  static void showAlert(title, body, context) {
    Alert(
      style: AlertStyle(
        animationType: AnimationType.grow,
      ),
      context: context,
      type: AlertType.info,
      title: title,
      desc: body,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Palette.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: prefix0.ColorConstants.mainColor1,
        ),
      ],
    ).show();
  }

  static void showSuccess(title, body, List<String> btnTitles, List<VoidCallback> actions, context) {
    List<DialogButton> arrayDialogBtns = [];
    for(int i = 0; i < btnTitles.length; i++) {
      var btn = DialogButton(
        child: Text(
          btnTitles[i],
          style: TextStyle(color: Palette.white, fontSize: 20),
        ),
        onPressed: actions[i],
        color: ColorConstants.mainColor1,
      );
      arrayDialogBtns.add(btn);
    }
    Alert(
      style: AlertStyle(
        animationType: AnimationType.grow,
      ),
      context: context,
      type: AlertType.success,
      title: title,
      desc: body,
      buttons: arrayDialogBtns,
    ).show();
  }


  static Future showErrorAlertCallBack(title, body, context, Function callBack) async {
    Alert(
      style: AlertStyle(
        animationType: AnimationType.grow,
      ),
      context: context,
      type: AlertType.error,
      title: title,
      desc: body,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Palette.white, fontSize: 20),
          ),
          onPressed: () {
            Navigator.pop(context);
            callBack();
          },
          color: prefix0.ColorConstants.mainColor1,
        ),
      ],
    ).show();
  }

  static void hideKeyboard(context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  static void push(context, dartName) {
    Navigator.push(
        context, MaterialPageRoute(builder: (BuildContext) => dartName));
  }

  static void pushWithNew(context, dartName) {
    Navigator.push(
        context, MaterialPageRoute(builder: (BuildContext) => dartName));
//    Navigator.p
  }

   static void showToast(context, message) {
    Toast.show(
        message, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }


    */
/*static Widget getPlaceholderText([String text = "Work in progress..."]){
    return Center(
      child: Text(text,
        style: TextStyle(fontSize: 25,
            color: ColorConstants.mainColor1),),
    );
  }
  }
}
*/
