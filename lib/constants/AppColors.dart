import 'package:flutter/material.dart';

class AppColors {
  AppColors._();


  static const Color kPrimaryBlue = const Color(0xff8111b5);
  static const Color kOrange = const Color(0xFFFE7822);
  static final Color kWhite = Colors.white;

  static const Color kAppYellow = const Color(0xFFFCE68F);
  static const Color kAppBlack = const Color(0xFF303131);
  static const Color kScaffoldBG = const Color(0xFFF8F8F8);
  static const Color kDisabledButtonColor = const Color(0xFFE1E1E1);

  static const Color kGreen = const Color(0xff8111b5);
  static const Color kBlue = const Color(0xff543ecf);
  static const Color kGrey = const Color(0xFF9F9F9F);
  static const Color kJerryAppColor = const Color(0xFF00606D);
  static const Color kToastColor = const Color(0xFF902dd7);
  static const Color kImageBorderColor = Colors.grey;
}

/*----------------New gradirent-----------------*/
class ColorConstants {
  String mainColor = "1";

//MARK:-
  static Color mainColor1 = Colors.purple;
  static Color kGradientTop = HexColor("7dabcc");
  static Color kGradientCenter = HexColor("a399d5");
  static Color kGradientBottom = HexColor("c6abe2");
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
