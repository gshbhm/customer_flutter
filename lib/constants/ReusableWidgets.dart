import 'package:flutter/material.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/UniversalFunctions.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shopkeeperapp/constants/UniversalProperties.dart';

Widget getAppThemedBGOne() {
  return Positioned.fill(
      child: new Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white, Color(0xff8111b5)]),
        ),
      ));
}

Widget getSpacer({double height, double width}) {
  return new SizedBox(
    height: height ?? 0.0,
    width: width ?? 0.0,
  );
}

Widget textFieldWidget({
  String hint,
  IconData icon,
  TextEditingController controller,
  FocusNode focusNode,
  bool obscureText,
  Function(String) validator,
  Function(String) onFieldSubmitted,
  TextCapitalization textCapitalization,
  TextInputAction inputAction,
  TextInputType keyboardType,
}) {
  return Theme(
    data: ThemeData(
      primaryColor: AppColors.kGreen,
    ),
    child: Container(
      child: TextFormField(
        style: TextStyle(color: Colors.black),
        keyboardType: keyboardType,
        controller: controller,
        obscureText: obscureText ?? false,
        focusNode: focusNode,
        validator: validator,
        maxLines: 1,
        textCapitalization: textCapitalization ?? TextCapitalization.none,
        onFieldSubmitted: onFieldSubmitted,
        cursorColor: AppColors.kGreen,
        textInputAction: inputAction ?? TextInputAction.next,
        decoration: InputDecoration(
          prefixIcon: Icon(icon),
          hintText: hint,
//          contentPadding: new EdgeInsets.symmetric(vertical: 25.0, horizontal: 10.0),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(32))),
        ),
      ),
    ),
  );
}

Widget getToast({@required String msg}) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: AppColors.kToastColor,
      textColor: Colors.white,
      fontSize: 16.0
  );
}
// Returns Ifinca themed flat button
  Widget getAppFlatButton({
    @required BuildContext context,
    Widget title,
    @required String titleText,
    @required VoidCallback onPressed,
    double height,
    Color backGroundColor,
    Color textColor,
  }) {
    // Defaults
    const Color defaultBackGroundColor = AppColors.kAppYellow;
    const Color defaultTextColor = Colors.white;

    final double _height = 42.0/*getScreenSize(context: context).height * 0.067*/;
    final double defaultHeight = _height > minimumDefaultButtonHeight
        ? _height
        : minimumDefaultButtonHeight;
    const Color defaultDisabledBackgroundColor = AppColors.kDisabledButtonColor;

    return Container(
      height: height ?? defaultHeight,
      width: getScreenSize(context: context).width,
      child: new FlatButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
        onPressed: onPressed,
        child: new Text(
          titleText,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 13.0,
              color: textColor ?? defaultTextColor),
        ) ??
            title,
        color: backGroundColor ?? defaultBackGroundColor,
        disabledColor: defaultDisabledBackgroundColor,
      ),
    );
  }