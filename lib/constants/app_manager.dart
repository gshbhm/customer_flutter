import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/CustomLoader.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/constants/api_type.dart';
import 'package:shopkeeperapp/webservices/authStoreUtils.dart';
import 'package:shopkeeperapp/webservices/constants.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_spinkit/flutter_spinkit.dart';

//import 'api_handler.dart';
//import 'api_urls.dart';

class ApiManager {
  static int sessionExpireCode = 406;

/* Login method */

  static Future<Map<String, dynamic>> login(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: kLogin,
        type: apiType.post,
        header: header,
        body: body);
  }

/* Contact Us method */
  static Future<Map<String, dynamic>> contactUs(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: kContactUs,
        type: apiType.post,
        header: header,
        body: body);
  }

  static Future<Map<String, dynamic>> getDashboardData(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: kHome + "?lat=29.685629&long=76.990547",
        type: apiType.get,
        header: header,
        body: body);
  }

// CustomLoader customLoader = CustomLoader();
  static Map<String, String> headerValues = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "device_id": "1234678",
    "device_type": "1",
    "app_version": "1.0",
  };

  static dynamic getMethod(
      BuildContext context, String url, String param) async {
    print("URL{$url}");
    if (await checkInternet(context) == true) {
      onLoading(context);
    } else {
      return;
    }

    AuthUtils.preferrences();

    var hdr = headerValues;
    print("{$url $hdr}");
    final response = await http.get('$url' + '$param', headers: headerValues);
    print('CheckEmail $response.body');
    final responseJson = json.decode(response.body);

    print("%%%%%%%%%%%%%%%%%%%%%%");
    print(response.statusCode);
    if (response.statusCode == sessionExpireCode) {
      sendToStartScreen(context);
    }
    print('CheckEmail1 $responseJson');
    Navigator.pop(context);
    return responseJson;
  }

//  static Future<bool> checkInternet(BuildContext context) async {
//    var connectivityResult = await (Connectivity().checkConnectivity());
//    if (connectivityResult == ConnectivityResult.mobile) {
//// I am connected to a mobile network.
//      return true;
//    } else if (connectivityResult == ConnectivityResult.wifi) {
//// I am connected to a wifi network.
//      return true;
//    } else {
//      Utils.showErrorAlert(
//          'Error', 'Please check your internet connection.', context);
//      return false;
//    }
//  }


  static Future<bool> checkInternet(BuildContext context) async {
    try {
      final result = await InternetAddress.lookup('google.com')
          .timeout(const Duration(seconds: 5));
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        Utils.showErrorAlert(
            'Error', 'Please check your internet connection.', context);
        return false;
      }
    } catch (_) {
      Utils.showErrorAlert(
          'Error', 'Please check your internet connection.', context);
      return false;
    }
  }

  static sendToStartScreen(BuildContext context) async {
    Map<String, String> data = {'userToken': ''};
    Map<String, dynamic> response = {'data': data};
    await AuthUtils.setUserToken(response);
   // Utils.push(context, StartScreen());
    Utils.showErrorAlert(
        'Error', 'Session expired!\nPlease login again.', context);
    return;
  }

  static onLoading(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          child: SpinKitCubeGrid(
            color: ColorConstants.mainColor1,
            size: 50.0,
          ),
        );
      },
    );
  }

/* HomePage method */
/* static Future<Map<String, dynamic>> Home({context,
   @required Map<String, String> header,
   @required Map<String, dynamic> body}) async {
   return await ApiHandler.hit(
       context: context,
       url: kContactUs,
       type: apiType.get,
       header: header,
       body: body);
  }*/
/*
  //Logout
  static Future<Map<String, dynamic>> logout(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.logout,
        type: apiType.get,
        header: header,
        body: body);
  } //Signup

  static Future<Map<String, dynamic>> signUp(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.signUp,
        type: apiType.post,
        header: header,
        body: body);
  }

  //verifyOtp
  static Future<Map<String, dynamic>> verifyOtp(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.verifyOtp,
        type: apiType.put,
        header: header,
        body: body);
  }

  //verifyEditProfileOtp
  static Future<Map<String, dynamic>> verifyEditProfileOtp(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.verifyEditProfilePhone,
        type: apiType.put,
        header: header,
        body: body);
  }

  //GetLoanList
  static Future<Map<String, dynamic>> getLoanList(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body,
      pageNumber: 1}) async {
    return await ApiHandler.hit(
        context: context,
        url:
            "${ApiUrl.getMyLoanREQList}?token=${MemoryManagement.getAccessToken()} ",
        type: apiType.get,
        header: header,
        body: body);
  }

  //getReadyToChat List
  static Future<Map<String, dynamic>> getReadyToChatList(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body,
      pageNumber: 1}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.getReadyToChatList + "?page=$pageNumber",
        type: apiType.get,
        header: header,
        body: body);
  }

  //search List
  static Future<Map<String, dynamic>> searchChatByLoan(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body,
      pageNumber: 1,
      text: ""}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.getReadyToChatList + "?page=${pageNumber}&search=$text",
        type: apiType.get,
        header: header,
        body: body);
  }

  //GetLoanList
  static Future<Map<String, dynamic>> editProfile(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.editProfile,
        type: apiType.put,
        header: header,
        body: body);
  }

  //add loan
  static Future<Map<String, dynamic>> requestLoan(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.addLoanRequest,
        type: apiType.post,
        header: header,
        body: body);
  }

  //add loan
  static Future<Map<String, dynamic>> getFAQList(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.faq,
        type: apiType.get,
        header: header,
        body: body);
  }

  static Future<Map<String, dynamic>> getLocation(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.locateUs,
        type: apiType.get,
        header: header,
        body: body);
  }

  static Future<Map<String, dynamic>> onMessageSend(
      {context,
      @required Map<String, String> header,
      @required Map<String, dynamic> body}) async {
    return await ApiHandler.hit(
        context: context,
        url: ApiUrl.onSend,
        type: apiType.post,
        header: header,
        body: body);
  }*/
}
