import 'dart:io' show File, InternetAddress, Platform;
import 'dart:convert';
import 'dart:core' as prefix0;
import 'dart:core';

//import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:shopkeeperapp/constants/AppColors.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'constants.dart';
import 'authStoreUtils.dart';
import 'dart:async';

const deviceTypeAndroid = 'Android';
const deviceTypeIphone = 'iPhone';

/// Simple delegating wrapper around a [Stream].
///
/// Subclasses can override individual methods, or use this to expose only the
/// [Stream] methods of a subclass.
///
/// Note that this is identical to [StreamView] in `dart:async`. It's provided
/// under this name for consistency with other `Delegating*` classes.
class DelegatingStream<T> extends StreamView<T> {
  DelegatingStream(Stream<T> stream) : super(stream);

  /// Creates a wrapper which throws if [stream]'s events aren't instances of
  /// `T`.
  ///
  /// This soundly converts a [Stream] to a `Stream<T>`, regardless of its
  /// original generic type, by asserting that its events are instances of `T`
  /// whenever they're provided. If they're not, the stream throws a
  /// [CastError].
  static Stream<T> typed<T>(Stream stream) => stream.cast();
}

class APIServices {
  static String getBasicAuth() {
    String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$username:$password'));
    return basicAuth;
  }

  static int sessionExpireCode = 406;

  static Map<String, String> headerValues = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "device_id": "1234678",
    "device_type": "1",
    "app_version": "1.0",
  };

  static dynamic getMethod(
      BuildContext context, String url, String param) async {
    print("URL{$url}");
    if (await checkInternet(context) == true) {
     // onLoading(context);
    } else {
      return;
    }

    AuthUtils.preferrences();

    var hdr = headerValues;
    print("{$url $hdr}");
    final response = await http.get('$url' + '$param', headers: headerValues);
    print('CheckEmail $response.body');
    final responseJson = json.decode(response.body);

    print("%%%%%%%%%%%%%%%%%%%%%%");
    print(response.statusCode);
    if (response.statusCode == sessionExpireCode) {
      // sendToStartScreen(context);
    }
    print('CheckEmail1 $responseJson');
   // Navigator.pop(context);
    return responseJson;
  }

//  static Future<bool> checkInternet(BuildContext context) async {
//    var connectivityResult = await (Connectivity().checkConnectivity());
//    if (connectivityResult == ConnectivityResult.mobile) {
//// I am connected to a mobile network.
//      return true;
//    } else if (connectivityResult == ConnectivityResult.wifi) {
//// I am connected to a wifi network.
//      return true;
//    } else {
//      Utils.showErrorAlert(
//          'Error', 'Please check your internet connection.', context);
//      return false;
//    }
//  }

  static Future<bool> checkInternet(BuildContext context) async {
    try {
      final result = await InternetAddress.lookup('google.com')
          .timeout(const Duration(seconds: 5));
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        Utils.showErrorAlert(
            'Error', 'Please check your internet connection.', context);
        return false;
      }
    } catch (_) {
      Utils.showErrorAlert(
          'Error', 'Please check your internet connection.', context);
      return false;
    }
  }

  static onLoading(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          child: SpinKitCubeGrid(
            color: ColorConstants.mainColor1,
            size: 50.0,
          ),
        );
      },
    );
  }

  static dynamic postMethod(
      BuildContext context, String url, Map<String, String> body) async {
    if (await checkInternet(context) == true) {
      // onLoading(context);
    } else {
      return;
    }

    print("postBody $body $url");
    AuthUtils.preferrences();
    print("headdvlaue$headerValues");
    var uri = url;
    try {
      final response = await http
          .post(uri, body: body, headers: {"Accept": "application/json"});

      print("%%%%%%%%%%%%%%%%%%%%%%");
      print(response.statusCode);
      //  Navigator.pop(context);
      if (response.statusCode == sessionExpireCode) {
        //sendToStartScreen(context);
      }
      final responseJson = json.decode(response.body);
      print('Register $responseJson');
      return responseJson;
    } catch (exception) {
      print(exception);
      if (exception.toString().contains('SocketException')) {
        return 'NetworkError';
      } else {
        return null;
      }
    }
  }
}
