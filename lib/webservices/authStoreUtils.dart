import 'package:shopkeeperapp/common/genratePassword.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:shopkeeperapp/language_handler/app_translations.dart';

class AuthUtils {
  static final String authTokenKey = 'token';
  static final String userTokenKey = 'userToken';

  // static final String selectedLanguageKey = 'selectedLanguageKey';
  static final String deviceWidthKey = 'deviceWidthKey';
  static String token = '';
  static String userToken = '';
  static String deviceToken = '';
  static final String deviceTokenKey = 'deviceTokenKey';

  static var mainColor = "1";

  static preferrences() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    SharedPreferences _sharedPreferences = await _prefs;
    String id = _sharedPreferences.getString(AuthUtils.authTokenKey);
    print('id $id');
    token = id;
  }

  static setDeviceToken(String token) async {
    deviceToken = token;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(deviceTokenKey, token);
  }

  static Future<String> getDeviceToken() async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    String device_token = _sharedPreferences.getString(deviceTokenKey);
    deviceToken = device_token;
    return device_token;
  }

  static String getToken(SharedPreferences prefs) {
    return prefs.getString(AuthUtils.authTokenKey);
  }

  static insertDetails(var response) async {
    final prefs = await SharedPreferences.getInstance();
    var data = response['data'];
    print(data['token']);
    AuthUtils.token = data['token'];
    prefs.setString(authTokenKey, data['token']);
  }

  static setUserToken(var response) async {
    userToken = response['data']['userToken'];
    final prefs = await SharedPreferences.getInstance();
    String userTokn = response['data']['userToken'];
    print(userTokn);
    prefs.setString(userTokenKey, userTokn);
    getUserToken();
  }

  static setUserIndToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(userTokenKey, token);
  }

  static Future<String> getUserToken() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    SharedPreferences _sharedPreferences = await _prefs;
    String userTken = _sharedPreferences.getString(AuthUtils.userTokenKey);
    print('userToken $userTken');
    userToken = userTken;
//    userToken  = userToken;
    return userToken;
  }

  static setPreference(var value, String key) async {
    print('value $value');
    mainColor = value;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<String> getPreference(String key) async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    SharedPreferences _sharedPreferences = await _prefs;
    String userToken = _sharedPreferences.getString(key);
    mainColor = "0";
    print('userToken1111 $userToken');
//    userToken  = userToken;
    return userToken;
  }

//  static localize(String token) async{
//    AppTranslations.of(context).text(token);
//  }
//

/*

  static insertSelectedLanguagae(String code) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(selectedLanguageKey, code);
  }

  static getSelectedLanguagae() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    SharedPreferences _sharedPreferences = await _prefs ;
    String userSelectedLangauge = _sharedPreferences.getString(selectedLanguageKey);
    return userSelectedLangauge;
  }


  static insertDeviceWidth(double width) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble(deviceWidthKey, width);
  }

  static getDeviceWidth() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    SharedPreferences _sharedPreferences = await _prefs ;
    double userSelectedLangauge = _sharedPreferences.getDouble(selectedLanguageKey);
    return userSelectedLangauge;
  }
*/

/*
  static String getSuggestedPassword() {
    var alphaNumaric = randomAlphaNumeric(3);
    var randomChar = randomeSpecialChar();
    var alpha = randomAlpha(4);
    print("randome$randomChar,alpha$alpha, alphaNumbari$alphaNumaric");

    var finalpassword = alpha + alphaNumaric + randomChar;

    //    var finalpassword = alphaNumaric + "A" + randomChar + "#" + alphaNumaric1 + "f" + alphaNumaric2;

    print('finalpassword $finalpassword');
    print(
        'alphaNumaric $alphaNumaric'); // random sequence of 10 alpha numeric i.e. aRztC1y32B
    print('randomCharV $randomChar');
    return finalpassword;
  }
*/
}
