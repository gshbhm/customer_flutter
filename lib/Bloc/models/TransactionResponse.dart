class TransactionResponse {
  int id;
  int userId;
  int amount;
  int discount;
  int finalAmount;
  String createdAt;
  int adminShare;
  int adminAmount;
  String code;
  String istDate;
  String custName;
  Store store;
  Customer customer;

  TransactionResponse(
      {this.id,
        this.userId,
        this.amount,
        this.discount,
        this.finalAmount,
        this.createdAt,
        this.adminShare,
        this.adminAmount,
        this.code,
        this.istDate,
        this.custName,
        this.store,
        this.customer});

  TransactionResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    amount = json['amount'];
    discount = json['discount'];
    finalAmount = json['final_amount'];
    createdAt = json['created_at'];
    adminShare = json['admin_share'];
    adminAmount = json['admin_amount'];
    code = json['code'];
    istDate = json['ist_date'];
    custName = json['cust_name'];
    store = json['store'] != null ? new Store.fromJson(json['store']) : null;
    customer = json['customer'] != null
        ? new Customer.fromJson(json['customer'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['amount'] = this.amount;
    data['discount'] = this.discount;
    data['final_amount'] = this.finalAmount;
    data['created_at'] = this.createdAt;
    data['admin_share'] = this.adminShare;
    data['admin_amount'] = this.adminAmount;
    data['code'] = this.code;
    data['ist_date'] = this.istDate;
    data['cust_name'] = this.custName;
    if (this.store != null) {
      data['store'] = this.store.toJson();
    }
    if (this.customer != null) {
      data['customer'] = this.customer.toJson();
    }
    return data;
  }
}

class Store {
  int id;
  String name;
  String email;
  String emailVerifiedAt;
  String createdAt;
  String updatedAt;
  String phone;
  num address;
  num city;
  num state;
  int role;
  int status;
  String description;
  int createdBy;
  int percent;
  String visiblePassword;
  String deletedAt;
  int categoryId;
  int ratio;
  String sName;
  String whatsappNumber;
  double latitude;
  double longitude;

  Store(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.createdAt,
        this.updatedAt,
        this.phone,
        this.address,
        this.city,
        this.state,
        this.role,
        this.status,
        this.description,
        this.createdBy,
        this.percent,
        this.visiblePassword,
        this.deletedAt,
        this.categoryId,
        this.ratio,
        this.sName,
        this.whatsappNumber,
        this.latitude,
        this.longitude});

  Store.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    phone = json['phone'];
    address = json['address'];
    city = json['city'];
    state = json['state'];
    role = json['role'];
    status = json['status'];
    description = json['description'];
    createdBy = json['created_by'];
    percent = json['percent'];
    visiblePassword = json['visible_password'];
    deletedAt = json['deleted_at'];
    categoryId = json['category_id'];
    ratio = json['ratio'];
    sName = json['s_name'];
    whatsappNumber = json['whatsapp_number'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['city'] = this.city;
    data['state'] = this.state;
    data['role'] = this.role;
    data['status'] = this.status;
    data['description'] = this.description;
    data['created_by'] = this.createdBy;
    data['percent'] = this.percent;
    data['visible_password'] = this.visiblePassword;
    data['deleted_at'] = this.deletedAt;
    data['category_id'] = this.categoryId;
    data['ratio'] = this.ratio;
    data['s_name'] = this.sName;
    data['whatsapp_number'] = this.whatsappNumber;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}

class Customer {
  int id;
  String name;
  String phone;
  String email;
  int state;
  int city;
  String address;
  String code;
  double points;
  int createdBy;
  String createdAt;
  String updatedAt;
  int status;
  Null deletedAt;
  Null parent;
  String dob;
  String whatsappNumber;
  Null refer;
  Null password;
  String visiblePassword;

  Customer(
      {this.id,
        this.name,
        this.phone,
        this.email,
        this.state,
        this.city,
        this.address,
        this.code,
        this.points,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.status,
        this.deletedAt,
        this.parent,
        this.dob,
        this.whatsappNumber,
        this.refer,
        this.password,
        this.visiblePassword});

  Customer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    state = json['state'];
    city = json['city'];
    address = json['address'];
    code = json['code'];
    points = json['points'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    status = json['status'];
    deletedAt = json['deleted_at'];
    parent = json['parent'];
    dob = json['dob'];
    whatsappNumber = json['whatsapp_number'];
    refer = json['refer'];
    password = json['password'];
    visiblePassword = json['visible_password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['state'] = this.state;
    data['city'] = this.city;
    data['address'] = this.address;
    data['code'] = this.code;
    data['points'] = this.points;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['status'] = this.status;
    data['deleted_at'] = this.deletedAt;
    data['parent'] = this.parent;
    data['dob'] = this.dob;
    data['whatsapp_number'] = this.whatsappNumber;
    data['refer'] = this.refer;
    data['password'] = this.password;
    data['visible_password'] = this.visiblePassword;
    return data;
  }
}