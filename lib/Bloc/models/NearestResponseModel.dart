
class NearestResponseModel {
  List<Data> data;

  NearestResponseModel({this.data});

  NearestResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  String email;
  Null emailVerifiedAt;
  String createdAt;
  String updatedAt;
  String phone;
  Null address;
  Null city;
  Null state;
  int role;
  int status;
  Null description;
  int createdBy;
  int percent;
  String visiblePassword;
  Null deletedAt;
  int categoryId;
  int ratio;
  String sName;
  String whatsappNumber;
  Null latitude;
  Null longitude;
  List<Images> image;
  Null stateObj;
  Null cityObj;

  Data(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.createdAt,
        this.updatedAt,
        this.phone,
        this.address,
        this.city,
        this.state,
        this.role,
        this.status,
        this.description,
        this.createdBy,
        this.percent,
        this.visiblePassword,
        this.deletedAt,
        this.categoryId,
        this.ratio,
        this.sName,
        this.whatsappNumber,
        this.latitude,
        this.longitude,
        this.image,
        this.stateObj,
        this.cityObj});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    phone = json['phone'];
    address = json['address'];
    city = json['city'];
    state = json['state'];
    role = json['role'];
    status = json['status'];
    description = json['description'];
    createdBy = json['created_by'];
    percent = json['percent'];
    visiblePassword = json['visible_password'];
    deletedAt = json['deleted_at'];
    categoryId = json['category_id'];
    ratio = json['ratio'];
    sName = json['s_name'];
    whatsappNumber = json['whatsapp_number'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    if (json['image'] != null) {
      image = new List<Images>();
      json['image'].forEach((v) {
        image.add(new Images.fromJson(v));
      });
    }
    stateObj = json['state_obj'];
    cityObj = json['city_obj'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['city'] = this.city;
    data['state'] = this.state;
    data['role'] = this.role;
    data['status'] = this.status;
    data['description'] = this.description;
    data['created_by'] = this.createdBy;
    data['percent'] = this.percent;
    data['visible_password'] = this.visiblePassword;
    data['deleted_at'] = this.deletedAt;
    data['category_id'] = this.categoryId;
    data['ratio'] = this.ratio;
    data['s_name'] = this.sName;
    data['whatsapp_number'] = this.whatsappNumber;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    if (this.image != null) {
      data['image'] = this.image.map((v) => v.toJson()).toList();
    }
    data['state_obj'] = this.stateObj;
    data['city_obj'] = this.cityObj;
    return data;
  }
}
class Images {
  int id;
  int userId;
  int type;
  String image;
  int status;
  String createdAt;
  String updatedAt;

  Images(
      {this.id,
        this.userId,
        this.type,
        this.image,
        this.status,
        this.createdAt,
        this.updatedAt});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    type = json['type'];
    image = json['image'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['type'] = this.type;
    data['image'] = this.image;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}