class DashboardResponse {
  List<Nearest> nearest;
  List<Newest> newest;
  List<Sale> sale;
  List<Fourth> fourth;

  DashboardResponse({this.nearest, this.newest, this.sale, this.fourth});

  DashboardResponse.fromJson(Map<String, dynamic> json) {
    if (json['nearest'] != null) {
      nearest = new List<Nearest>();
      json['nearest'].forEach((v) {
        nearest.add(new Nearest.fromJson(v));
      });
    }
    if (json['newest'] != null) {
      newest = new List<Newest>();
      json['newest'].forEach((v) {
        newest.add(new Newest.fromJson(v));
      });
    }
    if (json['sale'] != null) {
      sale = new List<Sale>();
      json['sale'].forEach((v) {
        sale.add(new Sale.fromJson(v));
      });
    }
    if (json['fourth'] != null) {
      fourth = new List<Fourth>();
      json['fourth'].forEach((v) {
        fourth.add(new Fourth.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.nearest != null) {
      data['nearest'] = this.nearest.map((v) => v.toJson()).toList();
    }
    if (this.newest != null) {
      data['newest'] = this.newest.map((v) => v.toJson()).toList();
    }
    if (this.sale != null) {
      data['sale'] = this.sale.map((v) => v.toJson()).toList();
    }
    if (this.fourth != null) {
      data['fourth'] = this.fourth.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Fourth {
  int id;
  String name;
  String email;
  Null emailVerifiedAt;
  String createdAt;
  String updatedAt;
  String phone;
  String address;
  int city;
  int state;
  int role;
  int status;
  Null description;
  int createdBy;
  int percent;
  String visiblePassword;
  Null deletedAt;
  int categoryId;
  int ratio;
  String sName;
  String whatsappNumber;
  double latitude;
  double longitude;
  int saleCount;
  List<Images> image;
  StateObj stateObj;
  CityObj cityObj;

  Fourth(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.createdAt,
        this.updatedAt,
        this.phone,
        this.address,
        this.city,
        this.state,
        this.role,
        this.status,
        this.description,
        this.createdBy,
        this.percent,
        this.visiblePassword,
        this.deletedAt,
        this.categoryId,
        this.ratio,
        this.sName,
        this.whatsappNumber,
        this.latitude,
        this.longitude,
        this.saleCount,
        this.image,
        this.stateObj,
        this.cityObj});

  Fourth.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    phone = json['phone'];
    address = json['address'];
    city = json['city'];
    state = json['state'];
    role = json['role'];
    status = json['status'];
    description = json['description'];
    createdBy = json['created_by'];
    percent = json['percent'];
    visiblePassword = json['visible_password'];
    deletedAt = json['deleted_at'];
    categoryId = json['category_id'];
    ratio = json['ratio'];
    sName = json['s_name'];
    whatsappNumber = json['whatsapp_number'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    saleCount = json['sale_count'];
    if (json['image'] != null) {
      image = new List<Images>();
      json['image'].forEach((v) {
        image.add(new Images.fromJson(v));
      });
    }
    stateObj = json['state_obj'] != null
        ? new StateObj.fromJson(json['state_obj'])
        : null;
    cityObj = json['city_obj'] != null
        ? new CityObj.fromJson(json['city_obj'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['city'] = this.city;
    data['state'] = this.state;
    data['role'] = this.role;
    data['status'] = this.status;
    data['description'] = this.description;
    data['created_by'] = this.createdBy;
    data['percent'] = this.percent;
    data['visible_password'] = this.visiblePassword;
    data['deleted_at'] = this.deletedAt;
    data['category_id'] = this.categoryId;
    data['ratio'] = this.ratio;
    data['s_name'] = this.sName;
    data['whatsapp_number'] = this.whatsappNumber;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['sale_count'] = this.saleCount;
    if (this.image != null) {
      data['image'] = this.image.map((v) => v.toJson()).toList();
    }
    if (this.stateObj != null) {
      data['state_obj'] = this.stateObj.toJson();
    }
    if (this.cityObj != null) {
      data['city_obj'] = this.cityObj.toJson();
    }
    return data;
  }
}

class Images{
  int id;
  int userId;
  int type;
  String image;
  int status;
  String createdAt;
  String updatedAt;

  Images(
      {this.id,
        this.userId,
        this.type,
        this.image,
        this.status,
        this.createdAt,
        this.updatedAt});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    type = json['type'];
    image = json['image'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['type'] = this.type;
    data['image'] = this.image;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class StateObj {
  int id;
  String title;
  int status;
  String createdAt;
  String updatedAt;

  StateObj({this.id, this.title, this.status, this.createdAt, this.updatedAt});

  StateObj.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class CityObj {
  int id;
  String title;
  int stateId;
  int status;
  String createdAt;
  String updatedAt;

  CityObj(
      {this.id,
        this.title,
        this.stateId,
        this.status,
        this.createdAt,
        this.updatedAt});

  CityObj.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    stateId = json['state_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['state_id'] = this.stateId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Nearest {
  int id;
  String name;
  String email;
  Null emailVerifiedAt;
  String createdAt;
  String updatedAt;
  String phone;
  String address;
  int city;
  int state;
  int role;
  int status;
  Null description;
  int createdBy;
  int percent;
  String visiblePassword;
  Null deletedAt;
  int categoryId;
  int ratio;
  String sName;
  String whatsappNumber;
  double latitude;
  double longitude;
  double distance;
  List<Images> image;
  StateObj stateObj;
  CityObj cityObj;

  Nearest(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.createdAt,
        this.updatedAt,
        this.phone,
        this.address,
        this.city,
        this.state,
        this.role,
        this.status,
        this.description,
        this.createdBy,
        this.percent,
        this.visiblePassword,
        this.deletedAt,
        this.categoryId,
        this.ratio,
        this.sName,
        this.whatsappNumber,
        this.latitude,
        this.longitude,
        this.distance,
        this.image,
        this.stateObj,
        this.cityObj});

  Nearest.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    phone = json['phone'];
    address = json['address'];
    city = json['city'];
    state = json['state'];
    role = json['role'];
    status = json['status'];
    description = json['description'];
    createdBy = json['created_by'];
    percent = json['percent'];
    visiblePassword = json['visible_password'];
    deletedAt = json['deleted_at'];
    categoryId = json['category_id'];
    ratio = json['ratio'];
    sName = json['s_name'];
    whatsappNumber = json['whatsapp_number'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    distance = json['distance'];
    if (json['image'] != null) {
      image = new List<Images>();
      json['image'].forEach((v) {
        image.add(new Images.fromJson(v));
      });
    }
    stateObj = json['state_obj'] != null
        ? new StateObj.fromJson(json['state_obj'])
        : null;
    cityObj = json['city_obj'] != null
        ? new CityObj.fromJson(json['city_obj'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['city'] = this.city;
    data['state'] = this.state;
    data['role'] = this.role;
    data['status'] = this.status;
    data['description'] = this.description;
    data['created_by'] = this.createdBy;
    data['percent'] = this.percent;
    data['visible_password'] = this.visiblePassword;
    data['deleted_at'] = this.deletedAt;
    data['category_id'] = this.categoryId;
    data['ratio'] = this.ratio;
    data['s_name'] = this.sName;
    data['whatsapp_number'] = this.whatsappNumber;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['distance'] = this.distance;
    if (this.image != null) {
      data['image'] = this.image.map((v) => v.toJson()).toList();
    }
    if (this.stateObj != null) {
      data['state_obj'] = this.stateObj.toJson();
    }
    if (this.cityObj != null) {
      data['city_obj'] = this.cityObj.toJson();
    }
    return data;
  }
}
/*

class Image {
  int id;
  int userId;
  int type;
  String image;
  int status;
  String createdAt;
  String updatedAt;

  Image(
      {this.id,
        this.userId,
        this.type,
        this.image,
        this.status,
        this.createdAt,
        this.updatedAt});

  Image.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    type = json['type'];
    image = json['image'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['type'] = this.type;
    data['image'] = this.image;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class StateObj {
  int id;
  String title;
  int status;
  String createdAt;
  String updatedAt;

  StateObj({this.id, this.title, this.status, this.createdAt, this.updatedAt});

  StateObj.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class CityObj {
  int id;
  String title;
  int stateId;
  int status;
  String createdAt;
  String updatedAt;

  CityObj(
      {this.id,
        this.title,
        this.stateId,
        this.status,
        this.createdAt,
        this.updatedAt});

  CityObj.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    stateId = json['state_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['state_id'] = this.stateId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
*/

class Newest {
  int id;
  String name;
  String email;
  String emailVerifiedAt;
  String createdAt;
  String updatedAt;
  String phone;
  String address;
  int city;
  int state;
  int role;
  int status;
  String description;
  int createdBy;
  int percent;
  String visiblePassword;
  String deletedAt;
  int categoryId;
  int ratio;
  String sName;
  String whatsappNumber;
  double latitude;
  double longitude;
  double distance;
  List<Images> image;
  StateObj stateObj;
  CityObj cityObj;

  Newest(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.createdAt,
        this.updatedAt,
        this.phone,
        this.address,
        this.city,
        this.state,
        this.role,
        this.status,
        this.description,
        this.createdBy,
        this.percent,
        this.visiblePassword,
        this.deletedAt,
        this.categoryId,
        this.ratio,
        this.sName,
        this.whatsappNumber,
        this.latitude,
        this.longitude,
        this.distance,
        this.image,
        this.stateObj,
        this.cityObj});

  Newest.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    phone = json['phone'];
    address = json['address'];
    city = json['city'];
    state = json['state'];
    role = json['role'];
    status = json['status'];
    description = json['description'];
    createdBy = json['created_by'];
    percent = json['percent'];
    visiblePassword = json['visible_password'];
    deletedAt = json['deleted_at'];
    categoryId = json['category_id'];
    ratio = json['ratio'];
    sName = json['s_name'];
    whatsappNumber = json['whatsapp_number'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    distance = json['distance'];
    if (json['image'] != null) {
      image = new List<Images>();
      json['image'].forEach((v) {
        image.add(new Images.fromJson(v));
      });
    }
    stateObj = json['state_obj'] != null
        ? new StateObj.fromJson(json['state_obj'])
        : null;
    cityObj = json['city_obj'] != null
        ? new CityObj.fromJson(json['city_obj'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['city'] = this.city;
    data['state'] = this.state;
    data['role'] = this.role;
    data['status'] = this.status;
    data['description'] = this.description;
    data['created_by'] = this.createdBy;
    data['percent'] = this.percent;
    data['visible_password'] = this.visiblePassword;
    data['deleted_at'] = this.deletedAt;
    data['category_id'] = this.categoryId;
    data['ratio'] = this.ratio;
    data['s_name'] = this.sName;
    data['whatsapp_number'] = this.whatsappNumber;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['distance'] = this.distance;
    if (this.image != null) {
      data['image'] = this.image.map((v) => v.toJson()).toList();
    }
    if (this.stateObj != null) {
      data['state_obj'] = this.stateObj.toJson();
    }
    if (this.cityObj != null) {
      data['city_obj'] = this.cityObj.toJson();
    }
    return data;
  }
}

class Sale {
  int id;
  String name;
  String email;
  Null emailVerifiedAt;
  String createdAt;
  String updatedAt;
  String phone;
  String address;
  int city;
  int state;
  int role;
  int status;
  Null description;
  int createdBy;
  int percent;
  String visiblePassword;
  Null deletedAt;
  int categoryId;
  int ratio;
  String sName;
  String whatsappNumber;
  double latitude;
  double longitude;
  int saleCount;
  List<Images> image;
  StateObj stateObj;
  CityObj cityObj;

  Sale(
      {this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.createdAt,
        this.updatedAt,
        this.phone,
        this.address,
        this.city,
        this.state,
        this.role,
        this.status,
        this.description,
        this.createdBy,
        this.percent,
        this.visiblePassword,
        this.deletedAt,
        this.categoryId,
        this.ratio,
        this.sName,
        this.whatsappNumber,
        this.latitude,
        this.longitude,
        this.saleCount,
        this.image,
        this.stateObj,
        this.cityObj});

  Sale.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    phone = json['phone'];
    address = json['address'];
    city = json['city'];
    state = json['state'];
    role = json['role'];
    status = json['status'];
    description = json['description'];
    createdBy = json['created_by'];
    percent = json['percent'];
    visiblePassword = json['visible_password'];
    deletedAt = json['deleted_at'];
    categoryId = json['category_id'];
    ratio = json['ratio'];
    sName = json['s_name'];
    whatsappNumber = json['whatsapp_number'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    saleCount = json['sale_count'];
    if (json['image'] != null) {
      image = new List<Images>();
      json['image'].forEach((v) {
        image.add(new Images.fromJson(v));
      });
    }
    stateObj = json['state_obj'] != null
        ? new StateObj.fromJson(json['state_obj'])
        : null;
    cityObj = json['city_obj'] != null
        ? new CityObj.fromJson(json['city_obj'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['city'] = this.city;
    data['state'] = this.state;
    data['role'] = this.role;
    data['status'] = this.status;
    data['description'] = this.description;
    data['created_by'] = this.createdBy;
    data['percent'] = this.percent;
    data['visible_password'] = this.visiblePassword;
    data['deleted_at'] = this.deletedAt;
    data['category_id'] = this.categoryId;
    data['ratio'] = this.ratio;
    data['s_name'] = this.sName;
    data['whatsapp_number'] = this.whatsappNumber;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['sale_count'] = this.saleCount;
    if (this.image != null) {
      data['image'] = this.image.map((v) => v.toJson()).toList();
    }
    if (this.stateObj != null) {
      data['state_obj'] = this.stateObj.toJson();
    }
    if (this.cityObj != null) {
      data['city_obj'] = this.cityObj.toJson();
    }
    return data;
  }
}