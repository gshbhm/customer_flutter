import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shopkeeperapp/Bloc/BaseBloc.dart';
import 'package:shopkeeperapp/allscreens/AccountScreen.dart';
import 'package:shopkeeperapp/constants/AppMessages.dart';
import 'package:shopkeeperapp/constants/CustomLoader.dart';
import 'package:shopkeeperapp/constants/Utils.dart';
import 'package:shopkeeperapp/constants/app_manager.dart';
import 'package:shopkeeperapp/constants/memory_management.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginBloc extends BaseBloc {
  final TextEditingController passwordController = new TextEditingController();
  final TextEditingController emailController = new TextEditingController();
  CustomLoader _customLoader = CustomLoader();

  //signIn method
  login(BuildContext context) async {
    _customLoader.showLoader(context);
    bool isConnected = await Utils.isConnectedToInternet();
    if (!isConnected ?? true) {
      _customLoader.hideLoader();
      getToast(msg: "${AppMessages.noInternetError}");
      return;
    }

    Map<String, dynamic> body = {
      "email": "${emailController.text}",
      "password": "${passwordController.text}"
    };

    //  try {
    var result =
        await ApiManager.login(header: {}, body: body, context: context);
    print("result :$result");
    if (result["status"] == 200) {
      String token = result != null ? "${result['token'] ?? ""}" : "";
      print("bloc Staus:$token");
      String msg = result != null ? result['message'] ?? "" : "";
      String name = result != null ? result['name'] ?? "" : "";
      String email = result != null ? result['email'] ?? "" : "";
      print(msg);
      if (token.isNotEmpty) {
        await MemoryManagement.init();
        MemoryManagement.setAccessToken(accessToken: token);
        MemoryManagement.setName(name: name);
        MemoryManagement.setEmail(email: email);
        _customLoader.hideLoader();
        Navigator.pushAndRemoveUntil(
            context,
            CupertinoPageRoute(builder: (context) => new AccountScreen()),
            (Route<dynamic> route) => false);
      }
    } else if (result["status"] == 422) {
      _customLoader.hideLoader();
      String msg = result["message"];
      getToast(msg: msg);
    }

    /*  } catch (e, st) {
      _customLoader.hideLoader();
      print("Exception  login:: $e \n stacktrace $st");
      getToast(msg: "${AppMessages.generalError}");
    }*/
  }

  Widget getToast({String msg}) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  dispose() {
    return null;
  }
}
