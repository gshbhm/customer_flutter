import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shopkeeperapp/allscreens/SplashScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.purple));
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        primaryColor: Colors.purple,
        primaryColorDark: Colors.purple,
        accentColor: Colors.purple,
        fontFamily: 'Raleway',
      ),
      home: SplashScreen(),
    ),
  );
}